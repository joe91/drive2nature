import play.Project._

name := "hello-play-java"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.2.2",
  "org.webjars.bower" % "bootstrap" % "3.3.4",
  "org.webjars.bower" % "angular" % "1.4.0-rc.2",
  "org.webjars.bower" % "angular-route" % "1.4.0-rc.2",
  "org.webjars.bower" % "angular-cookies" % "1.4.0-rc.2",
  "org.webjars.bower" % "angular-sanitize" % "1.4.0-rc.2",
  "org.webjars.bower" % "ng-file-upload" % "5.0.9",
  "org.webjars.bower" % "angular-bootstrap" % "0.13.0",
  "org.mindrot" % "jbcrypt" % "0.3m")

libraryDependencies ++= Seq(
  javaJdbc,
  "org.xerial" % "sqlite-jdbc" % "3.8.6",
  javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  "org.hibernate" % "hibernate-entitymanager" % "4.3.9.Final" // replace by your jpa implementation
)


playJavaSettings
