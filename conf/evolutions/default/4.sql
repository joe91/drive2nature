# --- !Ups

CREATE TABLE `Trip`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `title` TEXT,
 `costs` REAL,
 `groupActivity` BOOLEAN,
 `intention` TEXT,
 `interest`TEXT,
 `remarks`TEXT,
 `onlyProposed` BOOLEAN
);

# --- !Downs

DROP TABLE `Trip`;