# --- !Ups

CREATE TABLE `Route`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `tripId`INTEGER,
 `startLocation` TEXT,
 `targetLocation` TEXT,
 `departureDate` DATE,
 `departureTime` DATE,
 `availableSeats` INTEGER,
 `carType` TEXT,
 `equipment` TEXT,
 `isSequelRoute` BOOLEAN,
 FOREIGN KEY (tripId) REFERENCES Trip (id)
);

# --- !Downs

DROP TABLE `Route`;