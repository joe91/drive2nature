# --- !Ups

CREATE TABLE `RatingAttribute`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `ratingType` TEXT,
 `stars` INTEGER,
 `ratingId` INTEGER,
 FOREIGN KEY (ratingId) REFERENCES Rating (id)
);

# --- !Downs

DROP TABLE `RatingAttribute`;