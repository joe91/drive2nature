# --- !Ups

CREATE TABLE `TripParticipant`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `userId`INTEGER,
 `tripId`INTEGER,
 `role` TEXT,
 `isCreator` BOOLEAN,
 FOREIGN KEY (userId) REFERENCES User (id),
 FOREIGN KEY (tripId) REFERENCES Trip (id)
);

# --- !Downs

DROP TABLE `TripParticipant`;