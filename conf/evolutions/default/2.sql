# --- !Ups

CREATE TABLE `Rating`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `userIdSubmitted`INTEGER,
 `userIdReceived`INTEGER,
 `remark` TEXT,
 FOREIGN KEY (userIdReceived) REFERENCES TripParticipant (id),
 FOREIGN KEY (userIdSubmitted) REFERENCES TripParticipant (id)
);

# --- !Downs

DROP TABLE `Rating`;