# --- !Ups

CREATE TABLE `User`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `firstName` TEXT,
 `lastName` TEXT,
 `mail` TEXT,
 `username` TEXT,
 `password` TEXT,
 `birthday` DATE,
 `picture` TEXT,
 `address` INTEGER,
 `phoneNumber` TEXT,
 UNIQUE (username),
 UNIQUE (mail),
  FOREIGN KEY (address) REFERENCES Address (id)
);

# --- !Downs

DROP TABLE `User`;