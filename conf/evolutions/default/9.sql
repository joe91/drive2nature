# --- !Ups

ALTER TABLE `Message`
ADD COLUMN `timestamp` DATETIME;
ALTER TABLE `Message`
ADD COLUMN `subject` TEXT;

# --- !Downs

ALTER TABLE `Message`
DROP COLUMN `timestamp`;
ALTER TABLE `Message`
DROP COLUMN `subject`;