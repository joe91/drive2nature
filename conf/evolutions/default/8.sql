# --- !Ups

CREATE TABLE `Address`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `street`TEXT,
 `housenumber`TEXT,
 `postcode` TEXT,
 `city` TEXT,
 `country` TEXT
);

# --- !Downs

DROP TABLE `Address`;