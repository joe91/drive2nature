# --- !Ups

CREATE TABLE `Message`(
 `id` INTEGER PRIMARY KEY AUTOINCREMENT,
 `sender`INTEGER,
 `receiver`INTEGER,
 `text` TEXT,
 `isRead` Boolean,
 FOREIGN KEY (sender) REFERENCES User (id),
 FOREIGN KEY (receiver) REFERENCES User (id)
);

# --- !Downs

DROP TABLE `Message`;