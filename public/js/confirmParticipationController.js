/**
 * Created by Philipp on 22.06.2015.
 */

drive2natureApp.controller('confirmParticipationController', ['$scope', '$http', '$routeParams', '$timeout', function ($scope, $http, $routeParams, $timeout) {

    $scope.template = {
        "header": "assets/templates/header.html",
        "breadcrumbs": "assets/templates/breadcrumbs.html"
    };
    $scope.breadcrumbs = [
        {"link": "#/myTrips", "text": "My Trips"},
        {"link": "#/confirmParticipation/" + $routeParams.id, "text": "Confirm Participation"}
    ];

    $http.get('api/pendingParticipants/' + $routeParams.id)
        .success(function (result) {
            $scope.pendingParticipants = result;

        })
        .error(function (data, status) {
            if (status == 403) {
                window.location.replace('/#/login');
            }
        });

    $scope.text = "";

    $scope.decline = function (id, firstname, lastname, text) {
        var message = {
            text: text
        };
        $http.post('api/tripParticipant/decline/' + id, message)
            .success(function () {
                $scope.successAccept = false;
                $scope.errorAccept = false;
                $scope.successDecline = true;
                $scope.successMessage = "You declined successfully the user " + firstname + " " + lastname;
                $timeout(function () {
                        window.location.reload()
                    },
                    1500
                );
            })
            .error(function (data, status) {
                $scope.successAccept = false;
                $scope.successDecline = false;
                $scope.errorDecline = true;
                $scope.errorMessage = "Declining the user " + firstname + " " + lastname + " was not possible";
                if (status == 403) {
                    window.location.replace('/#/login');
                }
            });
    };

    $scope.accept = function (id, firstname, lastname, text) {
        var message = {
            text: text
        };
        $http.post('api/tripParticipant/accept/' + id, message)
            .success(function () {
                $scope.errorDecline = false;
                $scope.successDecline = false;
                $scope.successAccept = true;
                $scope.successMessage = "You accepted successfully the user " + firstname + " " + lastname;
                $timeout(function () {
                        window.location.reload()
                    },
                    1500
                );
            })
            .error(function (data, status) {
                $scope.successAccept = false;
                $scope.successDecline = false;
                $scope.errorAccept = true;
                if (status == 403) {
                    window.location.replace('/#/login');
                }
                $scope.errorMessage = "Accepting the user " + firstname + " " + lastname + " was not possible." +
                    " The User is already accepted or you don't have enough seats.";

            });
    };


}]);