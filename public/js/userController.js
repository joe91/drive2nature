/**
 * Created by Philipp on 20.05.2015.
 */


//------------

drive2natureApp.controller('userController', ['$scope', '$http', '$routeParams', '$timeout', function ($scope, $http, $routeParams, $timeout) {

    $scope.template = {
        "header": "assets/templates/header.html",
        "breadcrumbs": "assets/templates/breadcrumbs.html"
    };
    $scope.breadcrumbs = [{"link": "#/user", "text": "Sign Up"}];

    $scope.success = false;
    $scope.user = {};

    $scope.save = function () {
        $http.post('/api/user', $scope.user)
            .success(function (result) {
                $scope.error = false;
                $scope.success = true;
                $timeout(function () {
                        window.location.replace('/#/login')
                    },
                    5000
                );
            })
            .error(function (data, status) { // data = message / status -> bad request
                $scope.success = false;
                $scope.error = true;
                $scope.errorMessage = data;
            })
    };

// select countries
    $scope.countries = [
        {name: 'Austria', code: 'AU'},
        {name: 'France', code: 'FR'},
        {name: 'Germany', code: 'GE'},
        {name: 'Italy', code: 'IT'},
        {name: 'Netherlands', code: 'NE'}

    ];
}]);

//------------ directive that impossibles that errors are first shown when the user leaves the input field

drive2natureApp.directive('ngFocus', [function () {
    var FOCUS_CLASS = "ng-focused";
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$focused = false;
            element.bind('focus', function (evt) { // even focused
                element.addClass(FOCUS_CLASS);
                scope.$apply(function () {
                    ctrl.$focused = true;
                });
            }).bind('blur', function (evt) { // event has lost focus
                element.removeClass(FOCUS_CLASS);
                scope.$apply(function () {
                    ctrl.$focused = false;
                });
            });
        }
    }
}]);

//------------directive to test whether something is unique

drive2natureApp.directive('ensureUnique', ['$http', function ($http) {
    return {
        require: 'ngModel',
        link: function (scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function () {
                $http.post('/api/check' + attrs.ensureUnique, scope.user)
                    .success(function (data, status, headers, cfg) {
                        c.$setValidity('unique', data);
                    }).error(function (data, status, headers, cfg) {
                        c.$setValidity('unique', false);
                    });
            });
        }
    }
}]);

//------------function to compare the password input of the user
var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue === scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

drive2natureApp.directive("compareTo", compareTo);