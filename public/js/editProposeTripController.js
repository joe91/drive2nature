/**
 * Created by Johannes on 21.05.2015.
 */
drive2natureApp.controller('editProposeTripController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "footer" : "assets/templates/footer.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };

    $scope.breadcrumbs = [{"link": "#/myTrips", "text" : "My Trips"},
        {"link": "#/editTrip", "text" : "Edit Trip"}];

    $scope.trip = {};

    $http.get ('/api/trip/'+ $routeParams.id)
        .success (function(result){

        console.log(result);
        $scope.trip = result;

        $scope.trip.startLocation1 = result.routes[0].startLocation;
        $scope.trip.targetLocation1 = result.routes[0].targetLocation;
        $scope.trip.departureDate1 = result.routes[0].departureDate;
        $scope.trip.groupActivityNew = result.groupActivity;
        $scope.trip.onlyProposedNew = true;



    })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.next = function(){
        $scope.step++;
    };

    $scope.prev = function () {
        $scope.step--;
    };

    $scope.confirmStep = function () {
        $scope.step++;
        $http.post('/api/changetrip', $scope.trip)
            .success(function(result){
                $scope.success = true;
            })
            .error(function(data, status){ // data = message / status -> bad request
                $scope.error = true;
                $scope.errorMessage = data;
            })
    };

    $scope.jumpTo = function(id){
        $scope.step = id;
    };


    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy'];

    $scope.step = 1;
    $scope.success = false;
    $scope.trip = {};

}]);

