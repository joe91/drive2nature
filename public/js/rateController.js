/**
 * Created by Philipp on 10.06.2015.
 */
drive2natureApp.controller('rateController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

   $scope.template = {
       "header" : "assets/templates/header.html",
       "breadcrumbs" : "assets/templates/breadcrumbs.html"
   };
    $scope.breadcrumbs = [
        {"link": "#/myTrips", text: "My Trips"},
        {"link": "#/rate", "text": "Evaluate"}
    ];

    $scope.rate = [];
    $scope.alreadyRated = [];
    $scope.success = false;

    $http.get('api/notRatedTripParticpants/' + $routeParams.id)

        .success(function(result){
            $scope.participants = result;
        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.max = 5;

    $scope.hoveringOver = function(value) {
        $scope.overStar = value;
    };

    $scope.confirm = function(id){
        var request = $scope.rate[id];
        request.receiver = id;
        request.trip = $routeParams.id;
        $http.post('api/createRating', request)
            .success(function(){
                $scope.success = true;
                for(var j = 0; j < $scope.participants.length; j++){
                    if($scope.participants[j].id == id){
                        $scope.participants[j].id = -1;
                        break;
                    }
                }
            })
            .error(function(data, status){
                if(status == 403){
                    window.location.replace('/#/login');
                }
            });
    }
}]);