/**
 * Created by Philipp on 07.06.2015.
 */

drive2natureApp.controller('headerController', ['$scope', '$cookies', '$location', '$http', function($scope, $cookies, $location, $http){
    var cookies = $cookies.getAll();
    $scope.logedIn = null;
    $scope.size = 0;
    $scope.unread = false;

    if(cookies.hasOwnProperty("firstName") && cookies.hasOwnProperty("lastName") && cookies.hasOwnProperty("picture")){
        $scope.logedIn = {
            firstName: cookies.firstName,
            lastName: cookies.lastName,
            picture: "/assets/images/defaultImage.png"
        };
        if(cookies.picture != ""){
            $scope.logedIn.picture = cookies.picture;
        }

        $http.get('api/getReceivedMessages')
            .success(function(result){
                $scope.size = result.size;
                if($scope.size > 0){
                    $scope.unread = true;
                }
            })
            .error(function(status, data){
                if(status == 403){
                    window.location.replace('/#/login');
                }
            });

        $http.get('api/pendingParticipants')
            .success(function (result) {
                $scope.pendingParticipants = result;
                if ($scope.pendingParticipants.length > 0) {
                    $scope.newRequest = true;
                }
                else{
                    $scope.newRequest = false;
                }
            })
            .error(function (data, status) {
                if(status == 403){
                    window.location.replace('/#/login');
                }
            });

    }
    $scope.path = $location.path();

    $scope.logout = function(){
        $http.get('api/logout')
            .success(function(){
                angular.forEach($cookies.getAll(), function (v, k) {
                    $cookies.remove(k);
                });
                window.location.replace('/');
            })
            .error(function(message){

            })
    };

    $scope.$on('MessageRead', function(event, size){
            $scope.size = size;
            if($scope.size > 0){
                $scope.unread = true;
            }
        else{
                $scope.unread = false;
            }
    });



}]);
