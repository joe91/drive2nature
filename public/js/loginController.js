/**
 * Created by Philipp on 22.05.2015.
 */
drive2natureApp.controller('loginController', ['$scope', '$http', '$cookies', function($scope, $http, $cookies){

   $scope.template = {
       "header" : "assets/templates/header.html",
       "breadcrumbs" : "assets/templates/breadcrumbs.html"
   };
    $scope.breadcrumbs = [{"link": "#/login", "text" : "Login"}];

    $scope.user = null;

    $scope.login = function(){
        $http.post('api/login', $scope.cred)
            .success(function(result){
                $scope.user = result;
                $cookies.put("id", result.id);
                $cookies.put("firstName", result.firstName);
                $cookies.put("lastName", result.lastName);
                $cookies.put("picture", result.picture);
                window.location.replace('#/');
            })
            .error(function(data, status){
                $scope.error = true;
                $scope.errorMessage = data;
            })
    };
}]);


