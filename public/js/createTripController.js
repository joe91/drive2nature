/**
 * Created by Philipp on 21.05.2015.
 */
drive2natureApp.controller('createTripController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };
    $scope.breadcrumbs = [{"link": "#/createTrip", "text" : "Create Trip"}];



    $http.get('/api/isLogedIn')
        .success(function(result){
        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.step = 1;
    $scope.trip = {};
    $scope.success = false;
    $scope.trip.isSequelRoute1 = false;
    $scope.trip.isSequelRoute2 = false;
    $scope.trip.groupActivity = false;
    $scope.trip.onlyProposed = false;



    $scope.next = function(){
        $scope.step++;
    };

    $scope.prev = function () {
        $scope.step--;
    };

    $scope.confirmStep = function () {
        if(!$scope.signup_form.$valid){
            $scope.error=true;
            return;
        }
        $scope.step++;
        //http request
        $http.post('/api/trip', $scope.trip)
            .success(function(result){
                $scope.success = true;
            })
            .error(function(data, status){ // data = message / status -> bad request
                $scope.error = true;
                $scope.errorMessage = data;
            })
    };


    $scope.jumpTo = function(id){
        $scope.step = id;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy'];




    $scope.mytime = new Date();

    $scope.hstep = 1;
    $scope.mstep = 15;

    $scope.options = {
        hstep: [1, 2, 3],
        mstep: [1, 5, 10, 15, 25, 30]
    };

    // function to check if date1 is before date2
    $scope.beforeDate = function(date1, date2){
        //return new Date(date1).getDate() < new Date(date2).getDate();
        return date1 < date2;
    };

    // function to check if date3 is same as date4
    $scope.sameDate = function(date3, date4){
        //return new Date(date4).getDate() == new Date(date3).getDate();
        return date3 == date4;
    };

    // function to check if time1 is before time2
    $scope.beforeTime = function(time1, time2){
        return time1 <= time2;
    }


}]);

