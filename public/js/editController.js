drive2natureApp.controller('editController', ['$scope','$http', '$routeParams', '$cookies', 'Upload',
    function($scope, $http, $routeParams, $cookies, Upload){

   $scope.template = {
       "header" : "assets/templates/header.html",
       "breadcrumbs" : "assets/templates/breadcrumbs.html"
   };
    $scope.breadcrumbs = [{"link": "#/profile", "text" : "Profile"},
        {"link": "#/edit", "text" : "Edit"}];

    $http.get('/api/isLogedIn')
        .success(function(result){
        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.rubric = $routeParams.setting;
    $scope.currentValue = $routeParams.value;

    $scope.user = {}; // otherwise json null error
    $scope.success = false;

    $scope.editp = function(){
            $http.post('/api/changep', $scope.user)
                .success(function (result) {
                    $scope.error = false;
                    $scope.success = true;
                    $scope.currentValue = $scope.user.phoneNumber;
                })
                .error(function (data, status) {
                    $scope.success = false;
                    $scope.error = true;
                    $scope.errorMessage = data;
                })
    };

    $scope.editm = function(){
        $http.post('/api/changem', $scope.user)
            .success(function (result) {
                $scope.error = false;
                $scope.success = true;
                $scope.currentValue = $scope.user.mail;
            })
            .error(function (data, status) {
                $scope.success = false;
                $scope.error = true;
                $scope.errorMessage = data;
            })
    };

    $scope.editpw = function() {
        $http.post('/api/changepw', $scope.user)
            .success(function (result) {
                $scope.error = false;
                $scope.success = true;
            })
            .error(function (data, status) {
                $scope.success = false;
                $scope.error = true;
                $scope.errorMessage = data;
            })
    };

    $scope.editad = function() {
        $http.post('/api/changead', $scope.user)
            .success(function (result) {
                $scope.error = false;
                $scope.success = true;
            })
            .error(function (data, status) {
                $scope.success = false;
                $scope.error = true;
                $scope.errorMessage = data;
            })
    };






    $scope.countries = [
        {name: 'Austria', code: 'AU'},
        {name: 'France', code: 'FR'},
        {name: 'Germany', code: 'GE'},
        {name: 'Italy', code: 'IT'},


    ];

    $scope.upload = function (file) {
        if (file != null && file != undefined) {
            Upload.upload({
                url: 'api/uploadPic',
                file: file
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            }).success(function (result) {
                $cookies.put("picture", result);
                window.location.reload();
            });
        }
    };
}]);

