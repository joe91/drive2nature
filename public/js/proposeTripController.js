/**
 * Created by Felix Maier on 10.06.2015.
 */

drive2natureApp.controller('proposeTripController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };
    $scope.breadcrumbs = [{"link": "#/proposeTrip", "text" : "Propose Trip"}];

    $http.get('/api/isLogedIn')
        .success(function(result){
        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.step = 1;
    $scope.trip = {};
    $scope.success = false;
    $scope.trip.groupActivity = false;
    $scope.trip.isSequelRoute1 = false;
    $scope.trip.isSequelRoute2 = false;
    $scope.trip.costs = null;
    $scope.trip.interest = null;
    $scope.trip.remarks = null;
    $scope.trip.departureTime1 = null;
    $scope.trip.departureTime2 = null;
    $scope.trip.startLocation2 = null;
    $scope.trip.targetLocation2 = null;
    $scope.trip.departureDate2 = null;
    $scope.trip.carType = null;
    $scope.trip.equipment = null;
    $scope.trip.availableSeats = 0;
    $scope.trip.onlyProposed = true;



    $scope.next = function(){
        $scope.step++;
    };

    $scope.prev = function () {
        $scope.step--;
    };

    $scope.confirmStep = function () {
        $scope.step++;
        //TODO http request
        $http.post('/api/trip', $scope.trip)
            .success(function(result){
                $scope.success = true;
                console.log(result);
            })
            .error(function(data, status){ // data = message / status -> bad request
                $scope.error = true;
                $scope.errorMessage = data;
                console.log(result);
            })
    };

    $scope.jumpTo = function(id){
        $scope.step = id;
    };


    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy'];



    $scope.mytime = new Date();

    $scope.hstep = 1;
    $scope.mstep = 15;

    $scope.options = {
        hstep: [1, 2, 3],
        mstep: [1, 5, 10, 15, 25, 30]
    };


}]);