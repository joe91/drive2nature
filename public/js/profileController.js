drive2natureApp.controller('profileController', ['$scope', '$http', '$routeParams', '$cookies', function ($scope, $http, $routeParams, $cookies) {

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };
    $scope.breadcrumbs = [{"link": "#/profile", "text": "Profile"}];


        $http.get('/api/getUser')
            .success(function (result) {
                $scope.user = result;
                if(result.picture == ""){
                    $scope.user.picture = "assets/images/defaultImage.png"
                }
            })
            .error(function (data, status) {
                if (status == 403) {
                    window.location.replace('/#/login');
                }
            });


        $http.get('/api/getAverageRatings')

            .success(function (result) {
                //empty Json
                if (result == "") {
                    $scope.errorR = true;
                    $scope.successA = false;
                    $scope.errorMessage = "You currently do not have any received ratings! Make some trips in order to get some :) ";
                }
                else {
                    $scope.ratings = result;
                    $scope.max = 5;
                    $scope.isReadonly = true;
                    $scope.successA = true;
                }
            })
            .error(function (data, status) {
                if (status == 404) {
                    $scope.error = true;
                    $scope.errorMessage = data;
                }
            });

    $http.get('/api/getReceivedRatings')
        .success(function (result) {
            //empty Json
            if (result == "") {
                $scope.errorR = true;
                $scope.success = false;
                $scope.errorMessage = "You currently do not have any received ratings! Make some trips in order to get some :) ";
            }
            else {
                $scope.errorR = false;
                $scope.success = true;
                $scope.receivedRatings = result;
                $scope.max = 5;
                $scope.isReadonly = true;
            }

        })
        .error(function (data, status) {
            if (status == 404) {
                $scope.errorR = true;
                $scope.errorMessage = data;
            }
        });

        $http.get('/api/getSubmittedRatings')
            .success(function (result) {
                //empty Json
                if (result == "") {
                    $scope.errorS = true;
                    $scope.errorMessage = "You currently do not have any submitted ratings! Rate the other people in order to create trust:) "
                }
                else {
                    $scope.submittedRatings = result;
                    $scope.max = 5;
                    $scope.isReadonly = true;
                }
            })
            .error(function (data, status) {
                if (status == 404) {
                    $scope.errorS = true;
                    $scope.errorMessage = data;
                }
            });

    }]);