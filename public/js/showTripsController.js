/**
 * Created by Johannes on 31.05.2015.
 */
drive2natureApp.controller('showTripsController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    $scope.template = {
        "header": "assets/templates/header.html",
        "breadcrumbs": "assets/templates/breadcrumbs.html"
    };

    $scope.breadcrumbs = [{"link": "#/showTrips", "text": "Search Results"}];

    $scope.trips = {};

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.departure = $routeParams.startLocation;
    $scope.target = $routeParams.targetLocation;
    $scope.date = $routeParams.date;

    $scope.clear = function () {
        $scope.date = undefined;
        $routeParams.date = undefined;
    };


    $http.get('/api/TripsRequest/' + $scope.departure + '/' + $scope.target + '/' + $scope.date)


        .success(function (result) {
            if ($scope.departure == undefined && $scope.target == undefined && $scope.date == undefined) {
                $scope.successAll = true;
                $scope.trips = result;
            }
            else {
                $scope.trips = result.trips;
                $scope.relatedTrips = result.relatedTrips;
                // if trip of related trip is alreday in trips
                if($scope.trips != undefined) {
                    if ($scope.relatedTrips != undefined) {
                        for (var i = 0; i < $scope.trips.length; i++) {
                            for (var j = 0; j < $scope.relatedTrips.length; j++) {
                                if ($scope.trips[i].id == $scope.relatedTrips[i].id) {
                                    $scope.relatedTrips.splice(i);
                                }
                            }
                        }
                    }
                }

                if ($scope.trips == undefined) {
                    $scope.size = 0;
                }
                else{
                    $scope.size = $scope.trips.length;
                }
                if ($scope.relatedTrips == undefined) {
                    $scope.relatedSize = 0;
                }
                else{
                    $scope.relatedSize = $scope.relatedTrips.length;
                }

                $scope.error = false;
                $scope.successAll = false
            }

        })
        .error(function (data, status) {
            $scope.trips = {};
            $scope.size = 0;
            if ($scope.relatedSize == undefined) {
                $scope.relatedSize = 0;
            }
            else {
                $scope.relatedSize = result.relatedSize;
                $scope.relatedTrips = result.relatedTrips;
            }
            $scope.error = true;
            $scope.successAll = false;
            $scope.errorMessage = data;

        });
}]);