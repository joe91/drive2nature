/**
 * Created by Philipp on 01.07.2015.
 */


drive2natureApp.controller('scrollController', ['$scope', '$location', '$anchorScroll',
    function ($scope, $location, $anchorScroll) {
        $scope.gotoTop = function () {
            $location.hash('top');

            $anchorScroll();
        };
    }]);

