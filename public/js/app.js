var drive2natureApp = angular.module('drive2natureApp', ['ngRoute', 'ui.bootstrap', 'ngCookies', 'ngSanitize', 'ngFileUpload']);

drive2natureApp.config(function($routeProvider){
    $routeProvider
        .when('/', {
            templateUrl: 'assets/templates/home.html',
            controller: 'homeController'
        })
        .when('/user', {
            templateUrl: 'assets/templates/user.html',
            controller: 'userController'
        })
        .when('/login', {
            templateUrl: 'assets/templates/login.html',
            controller: 'loginController'
        })
        .when('/createTrip', {
            templateUrl: 'assets/templates/createTrip.html',
            controller: 'createTripController'
        })
        .when('/editTrip/:id', {
            templateUrl: 'assets/templates/editTrip.html',
            controller: 'editTripController'
        })
        .when('/deleteTrip/:id',{
            templateUrl: 'assets/templates/deleteTrip.html',
            controller: 'deleteTripController'
        })
        .when('/proposeTrip', {
            templateUrl: 'assets/templates/proposeTrip.html',
            controller: 'proposeTripController'
        })
        .when('/editProposedTrip/:id', {
            templateUrl: 'assets/templates/editProposedTrip.html',
            controller: 'editProposeTripController'
        })
        .when('/findTrip', {
            templateUrl: 'assets/templates/findTrip.html',
            controller: 'findTripController'
        })
        .when('/rating', {
            templateUrl: 'assets/templates/rating.html',
            controller: 'ratingController'
        })
        .when('/about',{
            templateUrl: 'assets/templates/about.html',
            controller: 'aboutController'
       })
        .when('/contact',{
            templateUrl: 'assets/templates/contact.html',
            controller: 'contactController'
        })
        .when('/imprint',{
            templateUrl: 'assets/templates/imprint.html',
            controller: 'imprintController'
        })
        .when('/showTrips',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips//',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips/:startLocation',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips//:targetLocation',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips///:date',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips/:startLocation/:targetLocation/',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips/:startLocation//:date',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips/:startLocation//',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips//:targetLocation/:date',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips/:startLocation/:targetLocation/:date',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips/:startLocation/:targetLocation//',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/showTrips///',{
            templateUrl: 'assets/templates/showTrips.html',
            controller: 'showTripsController'
        })
        .when('/rate/:id', {
            templateUrl: 'assets/templates/rate.html',
            controller: 'rateController'
        })
        .when('/profile',{
            templateUrl: 'assets/templates/profile.html',
            controller: 'profileController'
        })
        .when('/profile/:id',{
            templateUrl: 'assets/templates/profile.html',
            controller: 'profileController'
        })
        .when('/edit/:setting/:value',{
            templateUrl: 'assets/templates/edit.html',
            controller: 'editController'
        })
        .when('/edit/:setting',{ // for empty fields and password
            templateUrl: 'assets/templates/edit.html',
            controller: 'editController'
        })
        .when('/deleteAccount',{
            templateUrl: 'assets/templates/delete.html',
            controller: 'deleteController'
        })
        .when('/trip/:id',{
            templateUrl: 'assets/templates/trip.html',
            controller: 'tripController'
        })
        .when('/profilePublic/:id',{
            templateUrl: 'assets/templates/profilePublic.html',
            controller: 'profilePublicController'
        })
        .when('/messages',{
            templateUrl: 'assets/templates/messages.html',
            controller: 'messagesController'
        })
        .when('/myTrips',{
            templateUrl: 'assets/templates/myTrips.html',
            controller: 'myTripsController'
        })
        .when('/confirmParticipation/:id',{
            templateUrl: 'assets/templates/confirmParticipation.html',
            controller: 'confirmParticipationController'
        })
        .when('/cancelTrip/:id',{
            templateUrl: 'assets/templates/cancelTrip.html',
            controller: 'cancelTripController'
        })
});


//------------ Directive to parse the date correct
drive2natureApp.directive('datepickerLocaldate', ['$parse', function ($parse) {
    var directive = {
        restrict: 'A',
        require: ['ngModel'],
        link: link
    };
    return directive;

    function link(scope, element, attr, ctrls) {
        var ngModelController = ctrls[0];

        // called with a JavaScript Date object when picked from the datepicker
        ngModelController.$parsers.push(function (viewValue) {
            // undo the timezone adjustment we did during the formatting
            if(!viewValue) {
                return undefined; //necessary for eliminating the date in searching a trip
            }
            viewValue.setMinutes(viewValue.getMinutes() - viewValue.getTimezoneOffset());
            // we just want a local date in ISO format
            return viewValue.toISOString().substring(0, 10);
        });

        // called with a 'yyyy-mm-dd' string to format
        ngModelController.$formatters.push(function (modelValue) {
            if (!modelValue) {
                return undefined;
            }
            // date constructor will apply timezone deviations from UTC (i.e. if locale is behind UTC 'dt' will be one day behind)
            var dt = new Date(modelValue);
            // 'undo' the timezone offset again (so we end up on the original date again)
            dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
            return dt;
        });
    }
}]);

//------------ Directive to parse the time correct
drive2natureApp.directive('timepickerLocaltime', ['$parse', function ($parse) {
    var directive = {
        restrict: 'A',
        require: ['ngModel'],
        link: link
    };
    return directive;

    function link(scope, element, attr, ctrls) {
        var ngModelController = ctrls[0];

        // called with a JavaScript Date object when picked from the datepicker
        ngModelController.$parsers.push(function (viewValue) {
            // undo the timezone adjustment we did during the formatting
            viewValue.setMinutes(viewValue.getMinutes() - viewValue.getTimezoneOffset());
            // we just want a local date in ISO format
            return viewValue.toISOString().substring(11, 16);

        });

        // called with a 'yyyy-mm-dd' string to format
        ngModelController.$formatters.push(function (modelValue) {
            if (!modelValue) {
                return undefined;
            }
            // date constructor will apply timezone deviations from UTC (i.e. if locale is behind UTC 'dt' will be one day behind)
            var dt = new Date(modelValue);
            // 'undo' the timezone offset again (so we end up on the original date again)
            dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
            return dt;
        });
    }
}]);

