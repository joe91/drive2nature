/**
 * Created by Felix Maier on 30.06.2015.
 */

drive2natureApp.controller('deleteTripController', ['$scope','$http', '$routeParams', '$timeout','$cookies', function($scope, $http, $routeParams, $timeout, $cookies){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };

    $scope.breadcrumbs = [{"link": "#/myTrips", "text" : "my Trips"},
        {"link": "#/deleteTrip", "text" : "Delete Trip"}];

     $scope.tripId = $routeParams.id;

    $http.get ('/api/trip/'+ $routeParams.id)
        .success (function(result){
        $scope.trip = result;
    })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.trip ={};

    $scope.deleteTrip = function(tripId){
        $http.delete('/api/deleteTrip/'+ tripId)
            .success(function (result) {
                $scope.error = false;
                $scope.success = true;
            })
            .error(function (data, status) {
                $scope.success = false;
                $scope.error = true;
                $scope.errorMessage = data;
            })
    }

}]);