drive2natureApp.controller('deleteController', ['$scope','$http', '$routeParams', '$timeout','$cookies', function($scope, $http, $routeParams, $timeout, $cookies){

   $scope.template = {
       "header" : "assets/templates/header.html",
       "breadcrumbs" : "assets/templates/breadcrumbs.html"
   };
    $scope.breadcrumbs = [{"link": "#/profile", "text" : "Profile"},
        {"link": "#/delete", "text" : "Delete Account"}];


    $http.get ('/api/getUser')
        .success (function(result){
        $scope.user = result;
    })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.user ={};

    $scope.deleteAccount = function(){
        $http.delete('/api/delete')
            .success(function (result) {
                $scope.error = false;
                $scope.success = true;

                angular.forEach($cookies.getAll(), function (v, k) {
                    $cookies.remove(k);
                });
                $timeout(function(){
                    window.location.replace('/')},
                    2500
                );
            })
            .error(function (data, status) {
                $scope.success = false;
                $scope.error = true;
                $scope.errorMessage = data;
            })
    }
}]);