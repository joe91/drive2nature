/**
 * Created by Philipp on 18.06.2015.
 */

drive2natureApp.controller('messagesController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };

    $scope.breadcrumbs = [
        {"link": "#/messages", "text": "Messages"}
    ];



    $http.get('api/getReceivedMessages')
        .success(function (result) {
            $scope.receivedMessages = result.messages;
            $scope.size = result.size;
            if ($scope.size > 0) {
                $scope.unread = true;
            }
        })
        .error(function (status, data) {
            $scope.error = true;
            $scope.errorMessage = data;
        });

    $http.get('api/getWrittenMessages')
        .success(function (result) {
            $scope.writtenMessages = result;
        })
        .error(function (status, data) {
            $scope.error = true;
            $scope.errorMessage = data;
        });

    $scope.message = {};
    // send message
    $scope.sendMessage = function (receiverId) {

        var message = {
            receiverId: receiverId,
            text: $scope.message.text,
            subject: "Answer "
        };
        $http.post('/api/sendMessage', message)
            .success(function (result) {
                $scope.errorM = false;
                $scope.successM = true;
                $scope.message.text = "";

            })
            .error(function (data, status) {
                $scope.successM = false;
                $scope.errorM = true;
                $scope.errorMessage = data;
            })
    };


    // set the IsRead on True and changes the color

    $scope.readMessage = function (id, isRead) {
        var message = {
            messageId : id
        };

        $http.post('/api/readMessage', message)
            .success(function (result) {
                //change color
                document.getElementById(id).className = "panel-default";
                // if unread, reduce number of unread
                if (!isRead) {
                    $scope.receivedMessages.isRead = true;
                    $scope.size -= 1;

                    $scope.$broadcast('MessageRead', $scope.size)
                }
                if ($scope.size == 0) {
                    $scope.unread = false;
                }

            })
            .error(function (data, status) {
                $scope.error = true;
                $scope.errorMessage = data;
                $scope.receivedMessages.isRead = false;
            })
    };


    // set the IsRead on True and changes the color
    $scope.deleteMessage = function (id) {

        $http.delete('/api/deleteMessage/' + id)
            .success(function () {
                window.location.reload();
                $scope.successD = true;

            })
            .error(function (status) {
                $scope.deleted = false;
                $scope.errorMessage = "The Message could not have been deleted"
            })
    }


}]);