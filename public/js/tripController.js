/**
 * Created by Philipp on 12.06.2015.
 */
drive2natureApp.controller('tripController', ['$scope', '$http', '$routeParams', '$cookies', function($scope, $http, $routeParams, $cookies){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };
    $scope.participation = false;
    $scope.participated = false;
    $scope.alreadyParticipating = false;

    $http.get('/api/trip/' + $routeParams.id)
        .success(function(result){
            $scope.trip = result;
            $scope.breadcrumbs = [
                {"link": "#/showTrips", "text" : "Trips"},
                {link: "#/trip/" + $scope.trip.id, text: $scope.trip.title}
            ];

            for (var i = 0; i < $scope.trip.tripParticipant.length; i++) {
                if($scope.currentUser == $scope.trip.tripParticipant[i].user.id){
                    $scope.alreadyParticipating = true;
                }
            }
        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });


    $http.get('/api/getAuthor/' + $routeParams.id)
        .success(function(result){
            $scope.author = result;
        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });


    $scope.participate = function(){
        $scope.participation ^= true;
    };

    $scope.confirm = function(){
        var request = {
            tripId : $scope.trip.id,
            text : $scope.remarks
        };
        $http.post('api/requestParticipation', request)
            .success(function(){
                $scope.participated = true;
            })
            .error(function(data, status){
                if(status == 403){
                    window.location.replace('/#/login');
                }
            })
    };

    $scope.currentUser = $cookies.get("id");

}]);