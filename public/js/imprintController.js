/**
 * Created by Johannes on 30.05.2015.
 */


drive2natureApp.controller('imprintController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };
    $scope.breadcrumbs = [{"link": "#/imprint", "text" : "Imprint"}];
}]);