/**
 * Created by Philipp on 26.05.2015.
 */
drive2natureApp.controller('homeController', ['$scope', '$http', '$routeParams','$location',function($scope, $http, $routeParams, $location){

     $scope.template = {
        "header" : "assets/templates/header.html"
    };
    $scope.myInterval = 5000;
    var slides = $scope.slides = [
            {image : "assets/images/berge_1500x500.jpg", text : "Image 1"},
            {image : "assets/images/flusswinter_1500x500.jpg", text : "Image 2"},
            {image : "assets/images/gebirgsbach_1500x500.jpg", text : "Image 3"}
    ];

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.clear = function () {
        $scope.date = null;
    };

    $scope.formats = ['dd-MMMM-yyyy']

}]);