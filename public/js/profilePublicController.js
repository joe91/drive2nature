drive2natureApp.controller('profilePublicController', ['$scope', '$http', '$routeParams','$cookies', function($scope, $http, $routeParams, $cookies){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };
    $scope.id = $routeParams.id;

    $http.get('/api/getProfile/'+ $scope.id)
        .success(function(result,data){
            $scope.user = result.user;
            $scope.averages = result.averages;
            $scope.receivedRatings = result.receivedRatings;
            $scope.max = 5;
            $scope.isReadonly = true;
            $scope.successA = true;



            if($scope.averages == undefined){
                $scope.errorA = true;
                $scope.errorMessage = "The User has currently no ratings";
            }
            $scope.breadcrumbs = [
                {link: "#/profilePublic/" + $scope.id, text:"Public Profile of "+ $scope.user.firstName + " " +$scope.user.lastName}
            ];
        })
        .error(function(data, status) {
                $scope.error = true;
                $scope.errorMessage = data;

        });

        $scope.calculateAge = function calculateAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    };

    // get current user to check that the user does not write himself a message
    $scope.currentUser = $cookies.get("id");

    $scope.message = {};

    $scope.sendMessage = function(){
        var message = {
            receiverId : $scope.user.id,
            text : $scope.message.text,
            subject : "private Message "
        };
        $http.post('/api/sendMessage',message)
            .success(function(result){
                $scope.successM = true;
                $scope.errorM = false;
                $scope.message.text = "";
            })
            .error(function(data, status){
                $scope.successM = false;
                $scope.errorM = true;
                $scope.errorMessage = data;
            })
    }




}]);