/**
 * Created by Philipp on 22.06.2015.
 */

drive2natureApp.controller('myTripsController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {

   $scope.template = {
       "header" : "assets/templates/header.html",
       "breadcrumbs" : "assets/templates/breadcrumbs.html"
   };
    $scope.breadcrumbs = [{"link": "#/myTrips", "text": "My Trips"}];

    $scope.participatingTrips = null;
    $scope.pastTrips = null;

    $http.get('api/createdTrips')
        .success(function(result){
            $scope.createdTrips = result;
        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.participating = function(){
        if($scope.participatingTrips == null){
            $http.get('api/participatingTrips')
                .success(function(result){
                    $scope.participatingTrips = result;
                })
                .error(function(data, status){
                    if(status == 403){
                        window.location.replace('/#/login');
                    }
                });
        }
    };



    $scope.past = function(){
        if($scope.pastTrips == null){
            $http.get('api/pastTrips')
                .success(function(result){
                    $scope.pastTrips = result;
                })
                .error(function(data, status){
                    if(status == 403){
                        window.location.replace('/#/login');
                    }
                });
        }
    }

}]);