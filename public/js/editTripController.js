/**
 * Created by Johannes on 21.05.2015.
 */
drive2natureApp.controller('editTripController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams){

    $scope.template = {
        "header" : "assets/templates/header.html",
        "breadcrumbs" : "assets/templates/breadcrumbs.html"
    };

    $scope.breadcrumbs = [{"link": "#/myTrips", "text" : "myTrips"},
        {"link": "#/editTrip", "text" : "Edit Trip"}];

    $scope.trip = {};

    $http.get ('/api/trip/'+ $routeParams.id)
        .success (function(result){
        $scope.trip = result;
        $scope.trip.startLocation1 = result.routes[0].startLocation;
        $scope.trip.targetLocation1 = result.routes[0].targetLocation;
        $scope.trip.departureDate1 = result.routes[0].departureDate;
        $scope.trip.groupActivityNew = result.groupActivity;
        $scope.trip.onlyProposedNew = false;

        if(!$scope.trip.onlyProposed) {

            $scope.trip.startLocation2 = result.routes[1].startLocation;
            $scope.trip.targetLocation2 = result.routes[1].targetLocation;
            $scope.trip.isSequelRoute2 = result.routes[1].isSequelRoute;
            $scope.trip.availableSeatsNew = result.routes[0].availableSeats;
            $scope.trip.carTypeNew = result.routes[0].carType;
            $scope.trip.equipmentNew = result.routes[0].equipment;
            $scope.trip.isSequelRoute1 = result.routes[0].isSequelRoute;
            $scope.trip.departureDate2 = result.routes[1].departureDate;

            $scope.trip.departureTime1 = new Date();
            var dateParts1 = result.routes[0].departureTime.split(':');
            $scope.trip.departureTime1.setHours(dateParts1[0]);

            $scope.trip.departureTime1.setMinutes(dateParts1[1]);
            $scope.trip.departureTime1.setSeconds(dateParts1[2]);


            $scope.trip.departureTime2 = new Date();
            var dateParts2 = result.routes[1].departureTime.split(':');
            $scope.trip.departureTime2.setHours(dateParts2[0]);
            $scope.trip.departureTime2.setMinutes(dateParts2[1]);
            $scope.trip.departureTime2.setSeconds(dateParts2[2]);

        }else{
            $scope.trip.costs = "";
            $scope.trip.remarks = "";
            $scope.trip.interest = "";

        }


        })
        .error(function(data, status){
            if(status == 403){
                window.location.replace('/#/login');
            }
        });

    $scope.step = 1;
    $scope.success = false;
    $scope.trip = {};


    $scope.next = function(){
        $scope.step++;
    };

    $scope.prev = function () {
        $scope.step--;
    };

    $scope.confirmStep = function (tripID) {debugger;
        if(!$scope.signup_form.$valid){
            $scope.error=true;
            return;
        }
        $scope.step++;
        //http request
        console.log($scope.trip);
        $http.post('/api/changetrip', $scope.trip)
            .success(function(result){
                $scope.success = true;
            })
            .error(function(data, status){ // data = message / status -> bad request
                $scope.error = true;
                $scope.errorMessage = data;
                //console.log(result);
            })
    };


    $scope.jumpTo = function(id){
        $scope.step = id;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy'];




    $scope.mytime = new Date();

    $scope.hstep = 1;
    $scope.mstep = 15;

    $scope.options = {
        hstep: [1, 2, 3],
        mstep: [1, 5, 10, 15, 25, 30]
    };

    // function to check if date1 is before date2
    $scope.beforeDate = function(date1, date2){
        //return new Date(date1).getDate() < new Date(date2).getDate();
        return date1 < date2;
    };

    // function to check if date3 is same as date4
    $scope.sameDate = function(date3, date4){
        //return new Date(date4).getDate() == new Date(date3).getDate();
        return date3 == date4;
    };

    // function to check if time1 is before time2
    $scope.beforeTime = function(time1, time2){
        return time1 <= time2;
    }



}]);

