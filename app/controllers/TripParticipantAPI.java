package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import play.db.jpa.Transactional;
import java.util.Collection;
import java.util.Iterator;


/**
 * Created by Philipp on 16.06.2015.
 */
public class TripParticipantAPI extends Controller {

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result requestParticipation(){
        JsonNode json = request().body().asJson();
        Trip trip = Trip.findById(json.findPath("tripId").asInt());
        User user = User.findById(Integer.parseInt(session().get("id")));
        TripParticipant tripParticipant = new TripParticipant(
                trip,
                user,
                null,
                null,
                false,
                Role.pending
        );
        tripParticipant.save();

        User author = User.findAuthorByTripId(trip.getId());
        String messageText = "<p>The user " + user.getUsername() + " has requested to take part in your trip '" + trip.getTitle() +
                "'<br>He/She wrote the following remarks:</p><p>" + json.findPath("text").asText() + "</p>";
        Message message = new Message(user, author, messageText, false, "Participation requested");
        message.save();
        return ok("Participation requested");
    }


    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result pendingTripParticipantsByTripId(Integer id){
        Collection<TripParticipant> tripParticipants = TripParticipant.findPendingByTripId(id);
        return ok(Json.toJson(tripParticipants));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result pendingTripParticipantsByUser(){
        User user = User.findById(Integer.parseInt(session().get("id")));
        Collection<TripParticipant> tripParticipants = TripParticipant.findPendingByUser(user.getId());
        return ok(Json.toJson(tripParticipants));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result cancelTripParticipant(Integer tripId){

        User user = User.findById(Integer.parseInt(session().get("id")));
        TripParticipant tripParticipant = TripParticipant.findByTripAndUserId(tripId,user.getId());
        User creator = TripParticipant.findCreator(tripId).getUser();

        String firstname = user.getFirstName();
        String lastname = user.getLastName();
        String title =  tripParticipant.getTrip().getTitle();
        Integer routeId = tripParticipant.getTrip().getId();

        tripParticipant.delete();

        Route.increaseSeatsForTripId(routeId, 1);

        JsonNode json = request().body().asJson();
        Message message = new Message(
                user,
                creator,
                "<p> The User " + firstname + " " + lastname +
                        " canceled the trip: " + title + "<br>" +
                        firstname + " wrote the following remarks:</p><p> " + json.findPath("text").asText() + "</p>",
                false,
                "Cancel Participation"
        );
        message.save();
        return ok("Participation canceled");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result declineTripParticipant(Integer tripParticipantId){
        TripParticipant tripParticipant = TripParticipant.findById(tripParticipantId);
        tripParticipant.delete();

        JsonNode json = request().body().asJson();
        Message message = new Message(
                User.findById(Integer.parseInt(session().get("id"))),
                tripParticipant.getUser(),
                "<p> Your request to take part in " + tripParticipant.getTrip().getTitle() + " was declined. <br>" +
                          tripParticipant.getUser().getFirstName() + " " + tripParticipant.getUser().getLastName() + " " +
                "wrote the following remarks:</p><p> " + json.findPath("text").asText() + "</p>",
                false,
                "Request declined"
        );
        message.save();
        return ok("Participation declined");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result acceptTripParticipant(Integer tripParticipantId){
        TripParticipant tripParticipant = TripParticipant.findById(tripParticipantId);


        Iterator<Route> routeIterator = tripParticipant.getTrip().getRoutes().iterator();
        Route route = routeIterator.next();
        if(tripParticipant.getTrip().isOnlyProposed() == false && route.getAvailableSeats() <= 0){
            return badRequest();
        }

        if(tripParticipant.getRole() == Role.passenger){
            return badRequest();
        }
        tripParticipant.setRole(Role.passenger);
        tripParticipant.update();



        Route.reduceSeatsForTripId(tripParticipant.getTrip().getId(), 1);

        JsonNode json = request().body().asJson();
        Message message = new Message(
                User.findById(Integer.parseInt(session().get("id"))),
                tripParticipant.getUser(),
                "<p> Your request to take part in " + tripParticipant.getTrip().getTitle() + " was accepted. <br>" +
                        tripParticipant.getUser().getFirstName() + " " + tripParticipant.getUser().getLastName() + " " +
                        "wrote the following remarks:</p><p> " + json.findPath("text").asText() + "</p>",
                false,
                "Request accepted"
        );
        message.save();
        return ok("Participation accepted");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result tripParticipantsNotRatedByLoggedInUser(Integer tripId){
        Collection<TripParticipant> tripParticipants = TripParticipant.findByTripIdNotRatedByUserId(tripId, Integer.parseInt(session().get("id")));
        return ok(participantsToJson(tripParticipants));
    }

    public static ObjectNode participantToJson(TripParticipant tripParticipant){
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ObjectNode json = Json.newObject();
        json.put("trip", Json.toJson(tripParticipant.getTrip()));
        json.put("user", Json.toJson(tripParticipant.getUser()));
        json.put("isCreator", tripParticipant.isCreator());
        json.put("role", String.valueOf(tripParticipant.getRole()));
        json.put("id", tripParticipant.getId());

        ArrayNode routes = jsonNodeFactory.arrayNode();
        Iterator<Route> routeIterator = tripParticipant.getTrip().getRoutes().iterator();
        while (routeIterator.hasNext()){
            Route route = routeIterator.next();
            routes.add(Json.toJson(route));
        }
        json.put("routes", routes);
        return json;
    }

    public static ArrayNode participantsToJson(Collection<TripParticipant> tripParticipants){
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ArrayNode result = jsonNodeFactory.arrayNode();
        Iterator<TripParticipant> tripIt = tripParticipants.iterator();
        while (tripIt.hasNext()){
            TripParticipant tripParticipant = tripIt.next();
            result.add(participantToJson(tripParticipant));
        }
        return result;
    }

}
