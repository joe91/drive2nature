package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Philipp on 28.06.2015.
 */
public class RatingApi extends Controller {

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result saveRating(){
        JsonNode json = request().body().asJson();
        Rating rating = new Rating(
                TripParticipant.findByUserIdAndTripId(Integer.parseInt(session().get("id")), json.findPath("trip").asInt()),
                TripParticipant.findById(json.findPath("receiver").asInt()),
                json.findPath("remarks").asText()
        );
        rating.save();

        for(int i = 0; i < RatingType.values().length; i ++){
            if(json.has(RatingType.values()[i].toString())){
                RatingAttribute ratingAttribute = new RatingAttribute(
                        RatingType.values()[i],
                        json.findPath(RatingType.values()[i].toString()).asInt(),
                        rating
                );
                ratingAttribute.save();
            }
        }
        return created("Rating created");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result getAverageRatings (){
        Collection<Object[]> ratings = RatingAttribute.averageOverallEvaluatedRating(Integer.parseInt(session().get("id")));
        JsonNode json = Json.toJson(ratings);

        if (json != null) {
            return ok(json);
        }
        if (json == null) {
            return ok("");
        }
        else return notFound();
       // return notFound("You currently do not have any received ratings! Make some trips in order to get some :) ");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result getReceivedRatings (){
        Collection<Rating> receivedRatings = TripParticipant.getReceivedRatings(Integer.parseInt(session().get("id")));

        if(receivedRatings != null) {
            JsonNode json = RatingsToJson(receivedRatings);
            return ok(json);
        }
        if(receivedRatings == null) {
            return ok("");
        }
        else
            return notFound();
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result getSubmittedRatings (){
        Collection<Rating> submittedRatings = TripParticipant.getSubmittedRatings(Integer.parseInt(session().get("id")));

        if(submittedRatings != null) {
            JsonNode json = RatingsToJson(submittedRatings);
            return ok(json);
        }
        if(submittedRatings == null) {
            return ok("");
        }
        else
            return notFound();
    }

    // Creating a json to get the information for the Rating Page
    public static ObjectNode RatingToJson(Rating rating){
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ObjectNode json = Json.newObject();
        json.put("id",rating.getId());
        json.put("remark",rating.getRemark());
        json.put("evaluator",Json.toJson(rating.getEvaluator()));
        json.put("receiver",Json.toJson(rating.getReceiver()));

        ArrayNode attributes = jsonNodeFactory.arrayNode();
        Iterator<RatingAttribute> ratingIterator = rating.getAttribute().iterator();
        while (ratingIterator.hasNext()){
            RatingAttribute ratingAttribute = ratingIterator.next();
            attributes.add(Json.toJson(ratingAttribute));
        }
        json.put("attribute", attributes);
        json.put("routes",Json.toJson(rating.getReceiver().getTrip().getRoutes()));

        Collection <User> user = TripParticipant.getFellowPassenger(rating.getReceiver().getTrip().getId(),rating.getReceiver().getUser().getId(),Integer.parseInt(session().get("id")));
        json.put("fellowPassenger",Json.toJson(user));
        return json;
    }

    public static ArrayNode RatingsToJson(Collection<Rating> ratings){
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ArrayNode result = jsonNodeFactory.arrayNode();
        Iterator<Rating> ratingIterator = ratings.iterator();
        while (ratingIterator.hasNext()){
            Rating rating = ratingIterator.next();
            result.add(RatingToJson(rating));
        }
        return result;
    }

}
