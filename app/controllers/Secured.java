package controllers;

import models.User;
import play.libs.Json;
import play.mvc.Http.*;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by Philipp on 25.05.2015.
 */
public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx){
        String idString = ctx.session().get("id");
        if(idString == null){
            return null;
        }
        Integer id = Integer.parseInt(idString);
        User user = User.findById(id);
        if(user == null){
            return null;
        }
        return user.getUsername();
    }

    @Override
    public Result onUnauthorized(Context ctx){
        return forbidden(Json.toJson("Permission denied"));
    }
}
