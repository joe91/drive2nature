package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;


import java.util.*;


/**
 * Created by Felix Maier on 29.05.2015.
 */
public class TripApi extends Controller {

    public static Result index() {
        return ok(views.html.main.render());
    }


    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result createTrip() {

        JsonNode json = request().body().asJson();
        Form<Trip> tripForm = Trip.formt.bind(json);
        Form<Route> routeForm = Route.formr.bind(json);

        if (tripForm.hasErrors()) {
            return badRequest(Json.toJson(tripForm.errors()));
        }
        if (routeForm.hasErrors()) {
            return badRequest(Json.toJson(routeForm.errors()));
        } else {


            Trip trip = new Trip(
                    json.findPath("title").asText(),
                    json.findPath("costs").asDouble(),
                    json.findPath("groupActivity").asBoolean(),
                    json.findPath("intention").asText(),
                    json.findPath("interest").asText(),
                    json.findPath("remarks").asText(),
                    json.findPath("onlyProposed").asBoolean()
            );

            trip.save();

            Route routeThere = new Route(
                    trip,
                    json.findPath("startLocation1").asText(),
                    json.findPath("targetLocation1").asText(),
                    json.findPath("departureDate1").asText(),
                    json.findPath("departureTime1").asText(),
                    json.findPath("availableSeats").asInt(),
                    json.findPath("carType").asText(),
                    json.findPath("equipment").asText(),
                    json.findPath("isSequelRoute1").asBoolean());


            routeThere.save();

            Route routeBack = new Route(
                    trip,
                    json.findPath("startLocation2").asText(),
                    json.findPath("targetLocation2").asText(),
                    json.findPath("departureDate2").asText(),
                    json.findPath("departureTime2").asText(),
                    json.findPath("availableSeats").asInt(),
                    json.findPath("carType").asText(),
                    json.findPath("equipment").asText(),
                    json.findPath("isSequelRoute2").asBoolean());

            routeBack.save();

            User user = User.findById(Integer.parseInt(session().get("id")));
            TripParticipant tripParticipant = new TripParticipant(trip, user, null, null, true, Role.driver);
            tripParticipant.save();

            return created(Json.toJson(trip));
        }
    }

    @Transactional
    public static Result findAllTrips() {
        Collection<Trip> trips = Trip.findAllTrips();
        ArrayNode result = tripsToJson(trips);
        return ok(result);
    }


    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result findById(Integer id) {
        Trip trip = Trip.findById(id);
        return ok(tripParticipantToJson(trip));
    }

    //function for treat the different search possibilties
    @Transactional
    public static Result findTrips(String departure, String target, String date) {

        Date convertedDate = Route.convert(date);
        ObjectNode json = Json.newObject();

        // no search parameter
        if (departure.equals("undefined") && target.equals("undefined") && date.equals("undefined")) {
            // so that de user sees only dates later than now
            Date dateNow = new Date();
            Collection<Trip> trips = Trip.findAllTripsFromNow(dateNow);
            if (trips != null) {
                return ok(tripsToJson(trips));
            } else {
                return notFound("We are sorry, but there is currently no available trip");
            }
        }
        // only date
        if (departure.equals("undefined") && target.equals("undefined")) {
            Collection<Trip> trips = Trip.findByDate(convertedDate);
            if (trips != null) {
                ArrayNode result = tripsToJson(trips);
                json.put("trips", result);
                return ok(json);
            } else {
                return notFound("We are sorry, but there is currently no available trip on this date");
            }
        }
        // only target
        if (departure.equals("undefined") && date.equals("undefined")) {
            // so that de user sees only dates later than now
            Date dateNow = new Date();
            Collection<Trip> trips = Trip.findByTarget(target, dateNow);
            if (trips != null) {
                ArrayNode result = tripsToJson(trips);
                json.put("trips", result);
                return ok(json);
            } else {
                return notFound("We are sorry, but there is currently no available trip to this target ");
            }
        }
        // only departure
        if (target.equals("undefined") && date.equals("undefined")) {
            // so that de user sees only dates later than now
            Date dateNow = new Date();
            Collection<Trip> trips = Trip.findByDeparture(departure, dateNow);
            if (trips != null) {
                ArrayNode result = tripsToJson(trips);
                json.put("trips", result);
                return ok(json);
            } else {
                return notFound("We are sorry, but there is currently no available trip from this location");
            }
        }

        // departure and Date --> with 2 search parameters, related search is possible
        if (target.equals("undefined")) {
            Collection<Trip> trips = Trip.findByStartLocationAndDate(departure, convertedDate);
            Date dateNow = new Date();
            Collection<Trip> relatedTrips = Trip.relatedStartLocationandDate(departure, convertedDate, dateNow);

            if (trips != null) {
                Integer size = trips.size();
                ArrayNode result = tripsToJson(trips);
                json.put("trips", result);
                if (relatedTrips != null) {
                    json.put("relatedTrips", tripsToJson(relatedTrips));
                }
                return ok(json);
            }
            if (trips == null && relatedTrips != null) {
                json.put("relatedTrips", tripsToJson(relatedTrips));
                return ok(json);
            } else {
                return notFound("We are sorry, but there is currently no available trip from this departure on this date");
            }
        }
        // target and Date
        if (departure.equals("undefined")) {
            Collection<Trip> trips = Trip.findByTargetLocationAndDate(target, convertedDate);
            Date dateNow = new Date();
            Collection<Trip> relatedTrips = Trip.relatedTargetLocationandDate(target, convertedDate, dateNow);
            if (trips != null) {
                ArrayNode result = tripsToJson(trips);
                json.put("trips", result);
                if (relatedTrips != null) {
                    json.put("relatedTrips", tripsToJson(relatedTrips));
                }
                return ok(json);
            }
            if (trips == null && relatedTrips != null) {
                json.put("relatedTrips", tripsToJson(relatedTrips));
                return ok(json);
            } else {
                return notFound("We are sorry, but there is currently no available trip to this target on this date");
            }
        }
        //Departure and Target
        if (date.equals("undefined")) {
            // so that de user sees only dates later than now
            Date dateNow = new Date();
            Collection<Trip> trips = Trip.findByPlaces(departure, target, dateNow);
            Collection<Trip> relatedTrips = Trip.relatedStartLocationandTargetLocation(departure, target, dateNow);
            if (trips != null) {
                ArrayNode result = tripsToJson(trips);
                json.put("trips", result);
                if (relatedTrips != null) {
                    json.put("relatedTrips", tripsToJson(relatedTrips));
                }
                return ok(json);
            }
            if (trips == null && relatedTrips != null) {
                json.put("relatedTrips", tripsToJson(relatedTrips));
                return ok(json);
            } else {
                return notFound("We are sorry, but there is currently no available trip between those targets");
            }
        }
        // All Constraints
        if (!departure.equals("undefined") && !target.equals("undefined") && !date.equals("undefined")) {
            Collection<Trip> trips = Trip.findByAll(departure, target, convertedDate);
            Date dateNow = new Date();
            Collection<Trip> relatedTrips = Trip.relatedTripsAll(departure, target, convertedDate, dateNow);

            if (trips != null) {
                ArrayNode result = tripsToJson(trips);
                json.put("trips", result);
                if (relatedTrips != null) {
                    json.put("relatedTrips", tripsToJson(relatedTrips));
                }
                return ok(json);
            }
            if (trips == null && relatedTrips != null) {
                json.put("relatedTrips", tripsToJson(relatedTrips));
                return ok(json);
            } else {
                return notFound("We are sorry, but there is currently no available trip between those locations on this date");
            }
        }
        return notFound("We are sorry, but there is currently no available trip!");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result findCreatedByUser() {
        Collection<Trip> trips = Trip.findFutureTripsByUser(Integer.parseInt(session().get("id")), true);
        return ok(tripParticipantsToJson(trips));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result findParticipatingTrips() {
        Collection<Trip> trips = Trip.findFutureTripsByUser(Integer.parseInt(session().get("id")), false);
        return ok(tripsToJson(trips));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result findPastTripsByUser() {
        Collection<Trip> trips = Trip.findPastTripsByUserId(Integer.parseInt(session().get("id")));
        return ok(tripsToJson(trips));
    }

    public static ObjectNode tripToJson(Trip trip) {
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ObjectNode json = Json.newObject();
        json.put("title", trip.getTitle());
        json.put("intention", trip.getIntention());
        json.put("costs", trip.getCosts());
        json.put("interest", trip.getInterest());
        json.put("remarks", trip.getRemarks());
        json.put("groupActivity", trip.isGroupActivity());
        json.put("onlyProposed", trip.isOnlyProposed());
        json.put("id", trip.getId());

        ArrayNode routes = jsonNodeFactory.arrayNode();
        Iterator<Route> routeIterator = trip.getRoutes().iterator();
        while (routeIterator.hasNext()) {
            Route route = routeIterator.next();
            routes.add(Json.toJson(route));
        }
        json.put("routes", routes);
        return json;
    }

    public static ArrayNode tripsToJson(Collection<Trip> trips) {
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ArrayNode result = jsonNodeFactory.arrayNode();
        Iterator<Trip> tripIt = trips.iterator();
        while (tripIt.hasNext()) {
            Trip trip = tripIt.next();
            result.add(tripToJson(trip));
        }
        return result;
    }


    // for showing trips including the Trip Participants
    public static ObjectNode tripParticipantToJson(Trip trip) {
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ObjectNode json = Json.newObject();
        json.put("title", trip.getTitle());
        json.put("intention", trip.getIntention());
        json.put("costs", trip.getCosts());
        json.put("interest", trip.getInterest());
        json.put("remarks", trip.getRemarks());
        json.put("groupActivity", trip.isGroupActivity());
        json.put("onlyProposed", trip.isOnlyProposed());
        json.put("id", trip.getId());
        json.put("tripParticipant", Json.toJson(trip.getParticipants()));
        Integer size = trip.getParticipants().size();
        json.put("size", size); // never 0


        ArrayNode routes = jsonNodeFactory.arrayNode();
        Iterator<Route> routeIterator = trip.getRoutes().iterator();
        while (routeIterator.hasNext()) {
            Route route = routeIterator.next();
            routes.add(Json.toJson(route));
        }
        json.put("routes", routes);
        return json;
    }

    public static ArrayNode tripParticipantsToJson(Collection<Trip> trips) {
        JsonNodeFactory jsonNodeFactory = JsonNodeFactory.instance;
        ArrayNode result = jsonNodeFactory.arrayNode();
        Iterator<Trip> tripIt = trips.iterator();
        while (tripIt.hasNext()) {
            Trip trip = tripIt.next();
            result.add(tripParticipantToJson(trip));
        }
        return result;
    }


    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result deleteTrip(Integer tripId) {

        Trip trip = Trip.findById(tripId);
        if (trip == null) {
            return notFound("Trip was not found");
        }
        trip.delete();
        return ok();
    }


    // TODO
    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result changeTrip() {
        JsonNode json = request().body().asJson();

        Integer tripId = json.findPath("id").asInt();
        Trip trip = Trip.findById(tripId);


        trip.setTitle(json.findPath("title").asText());
        trip.setCosts(json.findPath("costs").asDouble());
        trip.setGroupActivity(json.findPath("groupActivityNew").asBoolean());
        trip.setIntention(json.findPath("intention").asText());
        trip.setRemarks(json.findPath("remarks").asText());
        trip.setInterest(json.findPath("interest").asText());
        trip.setOnlyProposed(json.findPath("onlyProposedNew").asBoolean());



        Iterator<Route> routeIterator = trip.getRoutes().iterator();

        Route route = routeIterator.next();

        route.setStartLocation(json.findPath("startLocation1").asText());
        route.setTargetLocation(json.findPath("targetLocation1").asText());
        route.setAvailableSeats(json.findPath("availableSeatsNew").asInt());
        route.setCarType(json.findPath("carTypeNew").asText());
        route.setDepartureDate(Route.convert(json.findPath("departureDate1").asText()));
        route.setDepartureTime(Route.convert2(json.findPath("departureTime1").asText()));
        route.setEquipment(json.findPath("equipmentNew").asText());
        route.setIsSequelRoute(json.findPath("isSequelRoute1").asBoolean());

        Route route2 = routeIterator.next();

        route2.setStartLocation(json.findPath("startLocation2").asText());
        route2.setTargetLocation(json.findPath("targetLocation2").asText());
        route2.setAvailableSeats(json.findPath("availableSeatsNew").asInt());
        route2.setCarType(json.findPath("carTypeNew").asText());
        route2.setDepartureDate(Route.convert(json.findPath("departureDate2").asText()));
        route2.setDepartureTime(Route.convert2(json.findPath("departureTime2").asText()));
        route2.setEquipment(json.findPath("equipmentNew").asText());
        route2.setIsSequelRoute(json.findPath("isSequelRoute2").asBoolean());

        trip.update();
        route.update();
        route2.update();

        return ok();
    }

}

