package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import org.mindrot.jbcrypt.BCrypt;


import java.io.File;
import java.util.Collection;


public class UserApi extends Controller {


    public static Result index() {
        return ok(views.html.main.render());
    }



    @Transactional
    public static Result createUser() {
        JsonNode json = request().body().asJson();
        Form<User> userForm = User.form.bind(json);
        Form<Address> addressForm = Address.form.bind(json);

        if (User.uniqueEmail(userForm.data().get("mail"))== false){
            return badRequest("There is already an account with this email");
        }
        if (User.uniqueUsername(userForm.data().get("username"))== false){
            return badRequest("This username is already in use!");
        }
        if (userForm.hasErrors()) {
            return badRequest("One ore more inputs of the mandatory fields are invalid! Please try again!");
        }
        if (addressForm.hasErrors()){
            return badRequest("One ore more inputs of the voluntary fields are invalid! Please try again!");
        }
        else {
            Address address = new Address(
                    json.findPath("street").asText(),
                    json.findPath("housenumber").asText(),
                    json.findPath("postcode").asText(),
                    json.findPath("city").asText(),
                    json.findPath("country").toString()
            );
            address.save();

            User user = new User(
                    json.findPath("firstName").asText(),
                    json.findPath("lastName").asText(),
                    json.findPath("mail").asText(),
                    json.findPath("username").asText(),
                    BCrypt.hashpw(json.findPath("password").asText(), BCrypt.gensalt()), // Encrypt password
                    json.findPath("birthday").asText(), //Date is read as text and parsed in the constructor
                    address,
                    json.findPath("phoneNumber").asText(),
                    ""
            );
            user.save();
            return created(Json.toJson(user));
        }
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result getUser(){

        String userId = session().get("id");
        User user = User.findById(Integer.parseInt(userId));
       if(user != null) {
           return ok(privateUserToJson(user));
       }
        else {
           return notFound("User not found");
       }
    }


    // for public profile
    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result getProfile(int id) {
        ObjectNode json = Json.newObject();
        User user = User.findById(id);

        if (user != null) {
            json.put("user", publicUserToJson(user));
            // if user exists, get the average rating of the user
            Collection<Object[]> average = RatingAttribute.averageOverallEvaluatedRating(id);
            // if user has ratings
            if (average == null) {
                return ok(json);
            } else {
                json.put("averages", Json.toJson(average));
                Collection<Rating> receivedRatings = TripParticipant.getReceivedRatings(id);
                if (receivedRatings != null) {
                    json.put("receivedRatings", RatingApi.RatingsToJson(receivedRatings));
                    return ok(json);
                } else {
                    return notFound();
                }
            }
        } else {
            return badRequest("It seems that the user is not registered anymore ");
        }

    }

    @Transactional
    public static Result findUsername(){
        JsonNode json = request().body().asJson();
        if(json != null){
            String username = json.findPath("username").asText();
            return ok(Json.toJson(User.uniqueUsername(username)));
        } else{
            return notFound("It seems that this User is not registered anymore");
        }
    }

    @Transactional
    public static Result findEmail(){
        JsonNode json = request().body().asJson();
        if(json != null){
            String mail = json.findPath("mail").asText();
            return ok(Json.toJson(User.uniqueEmail(mail)));
        } else{
            return notFound("This email does not exist!");
        }
    }

    @Transactional
    public static Result login() {
        JsonNode json = request().body().asJson();

        String username = json.findPath("username").asText();
        String password = json.findPath("password").asText();

        User user = User.findByUsername(username);

            if (user != null) {
                if (BCrypt.checkpw(password, user.getPassword())) {  // decrypt Password
                    user.login(username, password);
                session().clear();
                session().put("id", ((Integer) user.getId()).toString());
                return ok(Json.toJson(user));
            }
                else{
                    return notFound("Your Username and password did not match! Please try again");
                }
            }
        else {
            return badRequest("Your username was not found! Please try again or sign-up");
        }
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result isLogedIn(){
        return ok("Logged in");
    }

    @Transactional
    public static Result logout(){
        session().clear();
        return ok("Logged out");
    }


    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result changePhoneNumber(){

        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        }
        User user = User.findById(Integer.parseInt(session().get("id")));
        if (user == null) {
            return notFound("Your user was not found");
        }

        String phoneNumber = json.findPath("phoneNumber").asText();
        user.setPhoneNumber(phoneNumber);
        user.update();
        return ok(Json.toJson(user));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result changeEmail(){

        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        }

        User user = User.findById(Integer.parseInt(session().get("id")));
        if (user == null) {
            return notFound("Your user was not found");
        }

        String email = json.findPath("mail").asText();
        user.setMail(email);
        user.update();
        return ok(Json.toJson(user));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result changePassword(){

        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        }
        User user = User.findById(Integer.parseInt(session().get("id")));
        if (user == null) {
            return notFound("Your user was not found");
        }
        String oldpassword = json.findPath("oldpassword").asText();

        // change password only if the user entered the correct old one
        if (BCrypt.checkpw(oldpassword, user.getPassword())) {  // decrypt Password
            String password = BCrypt.hashpw(json.findPath("password").asText(), BCrypt.gensalt());
            user.setPassword(password);
            user.update();
            return ok(Json.toJson(user));
        }

        return badRequest("Your old Password was not correct");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result changeLastName(){

        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        }
        User user = User.findById(Integer.parseInt(session().get("id")));
        if (user == null) {
            return notFound("Your user was not found");
        }
        String lastName = json.findPath("lastName").asText();



        user.setLastName(lastName);
        user.update();
        return ok(Json.toJson(user));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result changeAddress(){

        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        }
        User user = User.findById(Integer.parseInt(session().get("id")));
        if (user == null) {
            return notFound("Your user was not found");
        }

        Address address = new Address(
                json.findPath("street").asText(),
                json.findPath("housenumber").asText(),
                json.findPath("postcode").asText(),
                json.findPath("city").asText(),
                json.findPath("country").toString()
        );

        user.setAddress(address);
        user.update();
        return ok(Json.toJson(user));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result getTripAuthor(Integer id){
        User user = User.findAuthorByTripId(id);
        return ok(Json.toJson(user));
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result deleteAccount(){

        User user = User.findById(Integer.parseInt(session().get("id")));
        if (user == null) {
            return notFound("Your user was not found");
        }
        user.delete();
        logout();
        return ok();
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result uploadPicture(){
        String id = session().get("id");
        User user = User.findById(Integer.parseInt(id));
        if(user.getPicture() != null && user.getPicture() != ""){
            File oldPicture = new File(user.getPicture());
            oldPicture.delete();
        }
        Http.MultipartFormData data = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart file = data.getFile("file");
        new File("public/images/user/" + id).mkdirs();
        file.getFile().renameTo(new File("public/images/user/" + id, file.getFilename()));
        String path = "assets/images/user/" + id + "/" + file.getFilename();
        user.setPicture(path);
        user.update();
        return ok(path);
    }

    // for public use (private things and password are not sent)
    public static ObjectNode publicUserToJson(User user){
        ObjectNode json = Json.newObject();
        json.put("id", user.getId());
        json.put("firstName", user.getFirstName());
        json.put("lastName", user.getLastName());
        json.put("username", user.getUsername());
        json.put("birthday", Json.toJson(user.getBirthday()));
        json.put("picture", Json.toJson(user.getPicture()));
        json.put("address", Json.toJson(user.getAddress()));
        return json;
    }

    // for private use (password not sent)
    public static ObjectNode privateUserToJson(User user){
        ObjectNode json = Json.newObject();
        json.put("id", user.getId());
        json.put("firstName", user.getFirstName());
        json.put("lastName", user.getLastName());
        json.put("username", user.getUsername());
        json.put("mail", user.getMail());
        json.put("phoneNumber", user.getPhoneNumber());
        json.put("birthday", Json.toJson(user.getBirthday()));
        json.put("picture", Json.toJson(user.getPicture()));
        json.put("address", Json.toJson(user.getAddress()));
        return json;
    }
}
