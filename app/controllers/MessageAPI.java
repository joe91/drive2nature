package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Message;
import models.User;
import play.data.Form;
import play.mvc.Result;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Security;

import play.db.jpa.Transactional;
import java.util.Collection;

/**
 * Created by Philipp on 16.06.2015.
 */
public class MessageAPI extends Controller {

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result sendMessage(){
        JsonNode json = request().body().asJson();

        User sender = User.findById(Integer.parseInt(session().get("id")));
        User receiver = User.findById(json.findPath("receiverId").asInt());
        String text = json.findPath("text").asText();
        String subject = json.findPath("subject").asText();

        Form<Message> messageForm = Message.formm.bind(json);
        if (messageForm.hasErrors()) {
            return badRequest("Your Message could not been sent! It must at least have 5 characters!");
        }

        Message message = new Message(sender, receiver, text, false, subject);
        message.save();
        return created();
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result findReceivedMessagesByUserId(){
        ObjectNode json = Json.newObject();
        Collection<Message> messages = Message.findReceivedMessagesByUserId(Integer.parseInt(session().get("id")));
        Collection<Message> unreadmessages = Message.findUnreadMessagesByUserId(Integer.parseInt(session().get("id")));
        if (messages != null) {
            json.put("messages", Json.toJson(messages));
            if (unreadmessages != null) {
                Integer size = unreadmessages.size();
                json.put("size", size);
            }
            return ok(json);
        }
        return notFound();
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result findWrittenMessagesByUserId(){
        Collection<Message> messages = Message.findWrittenMessagesByUserId(Integer.parseInt(session().get("id")));
        if(messages != null) {
            return ok(Json.toJson(messages));
        }
        return notFound();
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result readMessage(){
        JsonNode json = request().body().asJson();
        Message message = Message.findById(json.findPath("messageId").asInt());
        message.setIsRead(true);
        message.update();
        return ok("Message marked as read");
    }

    @Transactional
    @Security.Authenticated(Secured.class)
    public static Result deleteMessage(Integer messageId){

        Message message = Message.findById(messageId);
        message.delete();
        return ok("Message deleted");
    }
}
