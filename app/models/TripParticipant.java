package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Joe on 21.05.15.
 */

@Entity
@Table(name = "TripParticipant")
public class TripParticipant {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;


    @ManyToOne
    @JoinColumn(name = "tripId")
    private Trip trip;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @OneToMany(mappedBy = "receiver")
    @JsonBackReference
    private Collection<Rating> feedback;

    @OneToMany(mappedBy = "evaluator")
    @JsonBackReference
    private Collection<Rating> ratings;

    @Column(name = "isCreator")
    private boolean isCreator;

    @Column(name = "role")
    private Role role;


    public TripParticipant() {
    }

    public TripParticipant(Trip trip, User user, Collection<Rating> feedback, Collection<Rating> ratings, boolean isCreator, Role role) {
        this.trip = trip;
        this.user = user;
        this.feedback = feedback;
        this.ratings = ratings;
        this.isCreator = isCreator;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Collection<Rating> getFeedback() {
        return feedback;
    }

    public void setFeedback(Collection<Rating> feedback) {
        this.feedback = feedback;
    }

    public Collection<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(Collection<Rating> ratings) {
        this.ratings = ratings;
    }

    public boolean isCreator() {
        return isCreator;
    }

    public void setIsCreator(boolean isCreator) {
        this.isCreator = isCreator;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


    public void save(){
        JPA.em().persist(this);
        return;
    }

    public void update(){
        JPA.em().merge(this);
        return;
    }

    public void delete(){
        JPA.em().remove(this);
    }

    public static TripParticipant findById(int id){
        TypedQuery<TripParticipant> query = JPA.em().createQuery("SELECT tp FROM TripParticipant tp WHERE tp.id = :id", TripParticipant.class).setParameter("id", id);
        return query.getSingleResult();
    }

    public static TripParticipant findCreator(int tripId){
        TypedQuery<TripParticipant> query = JPA.em().createQuery("SELECT tp FROM TripParticipant tp WHERE tp.trip.id = :tripId and tp.isCreator = true", TripParticipant.class).setParameter("tripId", tripId);
        return query.getSingleResult();
    }

    public static TripParticipant findByTripAndUserId(Integer tripId, Integer userId){
        TypedQuery<TripParticipant> query = JPA.em().createQuery(
                "SELECT tp FROM TripParticipant tp WHERE tp.user.id = :userId AND tp.trip.id = :tripId",
                TripParticipant.class).setParameter("tripId", tripId).setParameter("userId",userId);
        return query.getSingleResult();
    }

    public static Collection<TripParticipant> findPendingByTripId(Integer tripId){
        TypedQuery<TripParticipant> query = JPA.em().createQuery(
                "SELECT tp FROM TripParticipant tp inner join fetch tp.user WHERE tp.role = :role AND tp.trip.id = :id",
                TripParticipant.class)
                .setParameter("role", Role.pending).setParameter("id", tripId);
        return query.getResultList();
    }

    public static Collection<TripParticipant> findPendingByUser(Integer userId){
        TypedQuery<TripParticipant> query = JPA.em().createQuery(
                "SELECT distinct tp2 FROM TripParticipant tp, TripParticipant tp2, Route ro WHERE tp2.role = :role AND tp.user.id = :id" +
                        " AND tp.isCreator = true AND tp.trip.id = tp2.trip.id AND tp.trip.id = ro.trip.id AND ro.departureDate >= :datenow ",
                TripParticipant.class)
                .setParameter("role", Role.pending).setParameter("id", userId).setParameter("datenow",new Date());
        return query.getResultList();
    }

    public static Collection<TripParticipant> findByTripIdNotRatedByUserId(Integer tripId, Integer userId){
        TypedQuery<TripParticipant> query = JPA.em().createQuery(
                "SELECT tp FROM TripParticipant tp inner join fetch tp.user WHERE tp.role <> :role AND tp.trip.id = :tripId " +
                        "AND tp.user.id <> :userId AND tp.id NOT IN (SELECT r.receiver.id FROM Rating r WHERE " +
                        "r.evaluator.user.id = :userId AND r.evaluator.trip.id = :tripId)",
                TripParticipant.class)
                .setParameter("role", Role.pending).setParameter("tripId", tripId).setParameter("userId", userId);
        return query.getResultList();
    }

    // function to get the received Ratings of an user
    public static Collection<Rating> getReceivedRatings (Integer id) {
        TypedQuery<Rating> query = JPA.em().createQuery("SELECT DISTINCT ra FROM  Rating ra, Route ro " +
                " WHERE ra.receiver.user.id = :id AND ra.receiver.trip.id = ra.evaluator.trip.id AND ra.receiver.trip.id = ro.trip.id ORDER BY ro.departureDate DESC", Rating.class).setParameter("id", id);
        List<Rating> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList;
        }
    }
    // function to get the submitted Ratings of an user
    public static Collection<Rating> getSubmittedRatings (Integer id) {
        TypedQuery<Rating> query = JPA.em().createQuery("SELECT DISTINCT ra FROM  Rating ra, Route ro" +
                " WHERE ra.evaluator.user.id = :id AND ra.receiver.trip.id = ra.evaluator.trip.id AND ra.evaluator.trip.id = ro.trip.id ORDER BY ro.departureDate DESC", Rating.class).setParameter("id", id);
        List<Rating> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList;
        }
    }

    // function to get the fellow Passengers
    public static Collection<User> getFellowPassenger (Integer tripId, Integer userId, Integer ownId) {
        TypedQuery<User> query = JPA.em().createQuery("SELECT DISTINCT tp.user FROM TripParticipant tp" +
                " WHERE tp.trip.id = :tripId AND tp.user.id <> :userId AND tp.user.id <> :ownId", User.class).setParameter("tripId", tripId).setParameter("userId",userId).setParameter("ownId",ownId);
        List<User> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList;
        }
    }

    public static TripParticipant findByUserIdAndTripId(int userId, int tripId){
        TypedQuery<TripParticipant> query = JPA.em().createQuery("SELECT tp FROM TripParticipant tp WHERE tp.user.id = :userId " +
                "AND tp.trip.id = :tripId", TripParticipant.class)
                .setParameter("userId", userId).setParameter("tripId", tripId);
        return query.getSingleResult();
    }

}












