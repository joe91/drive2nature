package models;

/**
 * Created by Joe on 21.05.15.
 */
public enum RatingType {
    punctuality, kindness, reliability, drivingSafety, overallSatisfaction
}
