package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.LazyCollectionOption;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Joe on 20.05.15.
 */

@Entity
@Table(name = "Rating")
public class Rating {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "userIdSubmitted")
    @JsonBackReference
    private TripParticipant evaluator;

    @ManyToOne
    @JoinColumn(name = "userIdReceived")
    @JsonBackReference
    private TripParticipant receiver;

    @Column (name = "remark")
    private String remark;

    @OneToMany (mappedBy = "rating", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonBackReference
    private Collection<RatingAttribute> attribute;


    public Rating() {
    }


    public Rating(TripParticipant evaluator, TripParticipant evaluated, String remark) {

        this.evaluator = evaluator;
        this.receiver = evaluated;
        this.remark = remark;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TripParticipant getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(TripParticipant evaluator) {
        this.evaluator = evaluator;
    }

    public TripParticipant getReceiver() {
        return receiver;
    }

    public void setReceiver(TripParticipant evaluated) {
        this.receiver = evaluated;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Collection<RatingAttribute> getAttribute() {
        return attribute;
    }

    public void setAttribute(Collection<RatingAttribute> attribute) {
        this.attribute = attribute;
    }

    public void save(){
        JPA.em().persist(this);
    }



}

