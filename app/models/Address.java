package models;

import play.data.Form;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Joe on 05.06.15.
 */

@Entity
@Table(name = "Address")
public class Address {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "street")
    @Constraints.MinLength(3)
    private String street;

    @Column(name = "housenumber")
    @Constraints.MaxLength(6)
    private String housenumber;

    @Column(name = "postcode")
    @Constraints.MaxLength(15)
    @Constraints.MinLength(3)
    private String postcode;

    @Column(name = "city")
    @Constraints.MinLength(3)
    private String city;

    @Column(name = "country")
    private String country;

    // for server validation
    public static Form<Address> form = Form.form(Address.class);

    public Address() {
    }

    public Address(String street, String housenumber, String postcode, String city, String country) {
        this.street = street;
        this.housenumber = housenumber;
        this.postcode = postcode;
        this.city = city;
        this.country = extract(country);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public void save() {
        JPA.em().persist(this);
        return;
    }

    public Address update() {
        return JPA.em().merge(this);

    }

    public void delete() {
        JPA.em().remove(this);
    }


    // extract the country
    public static String extract(String country) {
        Pattern pattern = Pattern.compile(":(.*?),"); // extract country
        Matcher matcher = pattern.matcher(country);
        String result = "";
        if (matcher.find()) {
            result = matcher.group(1);
            result = result.substring(1, result.length() - 1); // eliminate ""
            return result;
        }
        return null;
    }
}

