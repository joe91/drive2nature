package models;

import play.data.Form;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Joe on 05.06.15.
 */
@Entity
@Table(name = "Message")
public class Message {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "sender")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "receiver")
    private User receiver;

    @Column(name = "text")
    @Constraints.MinLength(5)
    @Constraints.Required
    private String text;

    @Column(name = "isRead")
    private Boolean isRead;

    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "subject")
    private String subject;

    // for server validation
    public static Form<Message> formm = Form.form(Message.class);

    public Message() {
    }

    public Message(User sender, User receiver, String text, Boolean isRead, String subject) {
        this.sender = sender;
        this.receiver = receiver;
        this.text = text;
        this.isRead = isRead;
        this.subject = subject;
        this.timestamp = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void save() {
        JPA.em().persist(this);
        return;
    }

    public void update() {
        JPA.em().merge(this);
    }


    public void delete() {
        JPA.em().remove(this);
    }

    public static Message findById(Integer id) {
        TypedQuery<Message> query = JPA.em().createQuery("SELECT m FROM Message m WHERE m.id = :id", Message.class)
                .setParameter("id", id);
        return query.getSingleResult();
    }


    public static Collection<Message> findReceivedMessagesByUserId(int id) {
        TypedQuery<Message> query = JPA.em().createQuery("SELECT m FROM Message m WHERE m.receiver.id = :id ORDER BY m.timestamp DESC", Message.class)
                .setParameter("id", id);
        if (query.getResultList() != null) {
            return query.getResultList();
        }
        return null;
    }

    public static Collection<Message> findUnreadMessagesByUserId(int id) {
        TypedQuery<Message> query = JPA.em().createQuery("SELECT m FROM Message m WHERE m.receiver.id = :id AND m.isRead = FALSE ORDER BY m.timestamp DESC", Message.class)
                .setParameter("id", id);
        if (query.getResultList() != null) {
            return query.getResultList();
        }
        return null;
    }

    public static Collection<Message> findWrittenMessagesByUserId(int id) {
        TypedQuery<Message> query = JPA.em().createQuery("SELECT m FROM Message m WHERE m.sender.id = :id ORDER BY m.timestamp DESC", Message.class)
                .setParameter("id", id);
        if (query.getResultList() != null) {
            return query.getResultList();
        }
        return null;
    }
}
