package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.typesafe.config.ConfigException;

import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import play.data.Form;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.*;

import play.data.validation.Constraints.Required;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;


@Entity
@Table(name = "Route")
public class Route {

    // for server validation
    public static Form<Route> formr = Form.form(Route.class);
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "tripId")
    public Trip trip;
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;
    @Column(name = "startLocation")
    //@Required
    //@Constraints.MinLength(3)
    private String startLocation;
    @Column(name = "targetLocation")
    //@Required
    //@Constraints.MinLength(3)
    private String targetLocation;
    @Column(name = "departureDate")
    //@Required
    @Temporal(TemporalType.DATE)
    private Date departureDate;
    @Column(name = "departureTime")
    //@Required
    @Temporal(TemporalType.TIME)
    private Date departureTime;
    @Column(name = "availableSeats")
    //@Required
    private int availableSeats;
    @Column(name = "carType")
    //@Required
    private String carType;
    @Column(name = "equipment")
    private String equipment;
    @Column(name = "isSequelRoute")
    private Boolean isSequelRoute;

    public Route() {
    }

    public Route(Trip trip, String startLocation, String targetLocation, String departureDate, String departureTime, int availableSeats, String carType, String equipment, Boolean isSequelRoute) {
        this.trip =  trip;
        this.startLocation = startLocation;
        this.targetLocation = targetLocation;
        this.departureDate = convert(departureDate);
        this.departureTime = convert2(departureTime);
        this.availableSeats = availableSeats;
        this.carType = carType;
        this.equipment = equipment;
        this.isSequelRoute = isSequelRoute;
    }


    public static Date convert (String date) {
        if (!Objects.equals(date, "undefined") && !Objects.equals(date, "null")) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dateConverted = null;
            try {
                dateConverted = formatter.parse(date);
            } catch (ParseException e) {

            }
            return dateConverted;
        }
        else return null;
    }


    public static Date convert2 (String time) {

        if (!Objects.equals(time, "undefined") && !Objects.equals(time, "null") ) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            Date result = null;
            try {
                result = format.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return result;
        }
        else return null;
    }

    public static void reduceSeatsForTripId(Integer tripId, Integer seats){
        Query query = JPA.em().createQuery("UPDATE Route r SET r.availableSeats = r.availableSeats - :seats WHERE r.trip.id = :id")
                .setParameter("seats", seats).setParameter("id", tripId);
        query.executeUpdate();
        return;
    }

    public static void increaseSeatsForTripId(Integer tripId, Integer seats){
        Query query = JPA.em().createQuery("UPDATE Route r SET r.availableSeats = r.availableSeats + :seats WHERE r.trip.id = :id")
                .setParameter("seats", seats).setParameter("id", tripId);
        query.executeUpdate();
        return;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getTargetLocation() {
        return targetLocation;
    }

    public void setTargetLocation(String targetLocation) {
        this.targetLocation = targetLocation;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public String getCarType() { return carType; }

    public void setCarType(String carType) { this.carType = carType; }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public String getEquipment() { return equipment; }

    public void setEquipment(String equipment) { this.equipment = equipment; }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Boolean getIsSequelRoute() {
        return isSequelRoute;
    }

    public void setIsSequelRoute(Boolean isSequelRoute) {
        this.isSequelRoute = isSequelRoute;
    }

    public void save(){
        JPA.em().persist(this);
        return;
    }

    public void delete() {
        JPA.em().remove(this);
    }

    public Route update() {
        return JPA.em().merge(this);

    }
}
