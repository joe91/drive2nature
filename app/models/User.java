package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import play.data.Form;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.jpa.*;


import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "firstName")
    @Required
    @MinLength(3)
    private String firstName;

    @Column(name = "lastName")
    @Required
    @MinLength(3)
    private String lastName;

    @Required
    @Email
    @Column(name = "mail", unique = true)
    private String mail;

    @Required
    @Column(name = "username",unique = true)
    @MinLength(3)
    private String username;

    @Column(name = "password")
    @Required
    @MinLength(6)
    private String password;

    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    @Required
    private Date birthday;

    @Column(name = "picture")
    private String picture;

    @Column(name = "phoneNumber")
    @MinLength(6)
    private String phoneNumber;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "address")
    private Address address;

    @OneToMany (cascade = CascadeType.ALL, mappedBy = "user") @JsonBackReference
    private Collection<TripParticipant> tripParticipants;

    @OneToMany (cascade = CascadeType.ALL, mappedBy = "sender") @JsonBackReference
    private Collection<Message> sentMessages;

    @OneToMany (cascade = CascadeType.ALL, mappedBy = "receiver") @JsonBackReference
    private Collection<Message> receivedMessages;




    // for server validation
    public static Form<User> form = Form.form(User.class);

    public User() {}


    public User(String firstName, String lastName, String mail, String username, String password, String birthday, Address address, String phoneNumber, String picture) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.username = username;
        this.password = password;
        this.birthday = convert(birthday);
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.picture = picture;

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date dateOfBirth) {
        this.birthday = dateOfBirth;
    }

    public Collection<TripParticipant> getTripParticipants() {
        return tripParticipants;
    }

    public void setTripParticipants(Collection<TripParticipant> tripParticipants) {
        this.tripParticipants = tripParticipants;
    }


    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Collection<Message> getSentMessages() {
        return sentMessages;
    }

    public void setSentMessages(Collection<Message> sentMessages) {
        this.sentMessages = sentMessages;
    }

    public Collection<Message> getReceivedMessages() {
        return receivedMessages;
    }

    public void setReceivedMessages(Collection<Message> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void save(){
      //  BCrypt.hashpw(this.password, BCrypt.gensalt());
        JPA.em().persist(this);
        return;
    }

    public User update() {
        return JPA.em().merge(this);

    }

    public void delete() {
        JPA.em().remove(this);
    }



    public static User findById(Integer id){
        TypedQuery<User> query = JPA.em().createQuery("SELECT u FROM User u WHERE u.id = :id", User.class).setParameter("id", id);
        List<User> resultList = query.getResultList();
        if(resultList.isEmpty()){
            return null;
        } else{
            return resultList.get(0);
        }
    }

    public static User findByUsername(String username){
        TypedQuery<User> query = JPA.em().createQuery("SELECT u FROM User u WHERE u.username = :username", User.class).setParameter("username", username);
        List<User> resultList = query.getResultList();
        if(resultList.isEmpty()){
            return null;
        } else{
            return resultList.get(0);
        }
    }

    public static Collection<User> findAllUsers(){
        TypedQuery<User> query = JPA.em().createQuery("SELECT u FROM User u", User.class);
        List<User> users = query.getResultList();
        return users;
    }

    public static User login(String username, String password){
        TypedQuery<User> query = JPA.em().createQuery("SELECT u FROM User u WHERE u.username = :username and u.password = :password", User.class);
        query.setParameter("username", username).setParameter("password", password);
        List<User> resultList = query.getResultList();
        if(resultList.isEmpty()){
            return null;
        }else{
            return resultList.get(0);
        }
    }

    public static boolean uniqueUsername(String username){
        TypedQuery<User> query = JPA.em().createQuery("SELECT u FROM User u WHERE u.username = :username", User.class).setParameter("username",username);
        List<User> result = query.getResultList();
        if(result.isEmpty()){
            return true;
        } else{
            return false;
        }

    }

    public static boolean uniqueEmail(String mail){
        TypedQuery<User> query = JPA.em().createQuery("SELECT u FROM User u WHERE u.mail = :mail", User.class).setParameter("mail",mail);
        List<User> result = query.getResultList();
        if(result.isEmpty()){
            return true;
        } else{
            return false;
        }

    }

    public static User findAuthorByTripId(int id){
        TypedQuery<User> query = JPA.em().createQuery("SELECT u FROM User u, TripParticipant tp WHERE u.id = tp.user.id AND tp.trip.id = :id AND tp.isCreator = true", User.class)
                .setParameter("id", id);
        return query.getSingleResult();
    }



    public static Date convert (String date) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date dateOfBirth = null;
            try {
                dateOfBirth = formatter.parse(date);
            } catch (ParseException e) {

            }
            return dateOfBirth;
        }
        else return null;
    }

}
