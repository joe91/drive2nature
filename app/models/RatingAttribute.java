package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Created by Joe on 20.05.15.
 */
@Entity
@Table(name = "RatingAttribute")
public class RatingAttribute {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;


    @Column(name = "ratingType")
    private RatingType type;

    @Column(name = "stars")
    private int stars;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "ratingId")
    private Rating rating;

    public RatingAttribute() {
    }

    public RatingAttribute(RatingType type, int stars, Rating rating) {
        this.type = type;
        this.stars = stars;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RatingType getType() {
        return type;
    }

    public void setType(RatingType type) {
        this.type = type;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }


    public void save(){
        JPA.em().persist(this);
        return;
    }

    public static Collection<Object[]> averageOverallEvaluatedRating(Integer id) {
       Query query = JPA.em().createQuery("SELECT ra.type, avg(ra.stars) FROM RatingAttribute ra " +
                "WHERE ra.rating.receiver.user.id = :id GROUP BY ra.type").setParameter("id", id);
        List<Object[]> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList;
        }
    }
}



