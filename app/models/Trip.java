package models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;
import com.fasterxml.jackson.databind.JsonNode;
import org.hibernate.annotations.GeneratorType;
import org.mindrot.jbcrypt.BCrypt;
import play.data.Form;
import play.data.validation.Constraints.Required;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Pattern;
import play.db.jpa.*;


import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Entity
@Table(name = "Trip")
public class Trip {

    // for server validation
    public static Form<Trip> formt = Form.form(Trip.class);


    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;
    @Column(name = "title")
    //@Required
    //@MinLength(3)
    private String title;
    @Column(name = "costs")
    //@Required
    private double costs;
    @Column(name = "groupActivity")
    private boolean groupActivity;
    @Column(name = "intention")
    private String intention;
    @Column(name = "interest")
    private String interest;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "onlyProposed")
    private boolean onlyProposed;

    @JsonManagedReference
    @OneToMany (mappedBy = "trip",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Collection<Route> routes;

    @JsonBackReference
    @OneToMany (mappedBy = "trip",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Collection<TripParticipant> participants;

    public Trip() {
    }

    public Trip(String title, double costs, boolean groupActivity, String intention, String interest, String remarks, boolean onlyProposed) {
        this.title = title;
        this.costs = costs;
        this.groupActivity = groupActivity;
        this.intention = intention;
        this.interest = interest;
        this.remarks = remarks;
        this.onlyProposed = onlyProposed;
    }

    public static Trip findById(int id){
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT t FROM Trip t WHERE t.id = :id", Trip.class).setParameter("id", id);
        List<Trip> trips = query.getResultList();
        if(trips.isEmpty()){
            return null;
        }
        return trips.get(0);
    }

    // returns all trips
    public static Collection<Trip> findAllTrips() {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT t FROM Trip t",Trip.class);
        List<Trip> trips = query.setMaxResults(50).getResultList();

        return trips;
    }
    // returns all trips later than now
    public static Collection<Trip> findAllTripsFromNow(Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.departureDate > :date " +
                        " AND (ro.availableSeats > 0 OR t.onlyProposed = true) ORDER BY ro.departureDate asc",
                Trip.class).setParameter("date", date);
        List<Trip> trips = query.setMaxResults(50).getResultList();

        return trips;
    }



    //search trip by departure
    public static Collection<Trip> findByDeparture(String departure, Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.startLocation like :departure" +
                " AND ro.departureDate > :date AND (ro.availableSeats > 0 OR t.onlyProposed = true) ORDER BY ro.departureDate asc",Trip.class);
        query.setParameter("departure", '%'+departure+'%').setParameter("date", date);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }

    //search trip by target
    public static Collection<Trip> findByTarget(String target, Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.targetLocation like :target " +
                " AND ro.departureDate > :date AND (ro.availableSeats > 0 OR t.onlyProposed = true) ORDER BY ro.departureDate asc ",Trip.class);
        query.setParameter("target",'%'+target+'%').setParameter("date", date);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }

    //search trip by locations
    public static Collection<Trip> findByPlaces(String departure, String target, Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.startLocation like :departure " +
                "and ro.targetLocation like :target AND ro.departureDate > :date AND (ro.availableSeats > 0" +
                " OR t.onlyProposed = true) ORDER BY ro.departureDate asc ",Trip.class);
        query.setParameter("departure", '%'+ departure + '%').setParameter("target", '%' + target+'%').setParameter("date", date);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }

    //search trip by Date
    public static Collection<Trip> findByDate(Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.departureDate = :date" +
                " AND (ro.availableSeats > 0 OR t.onlyProposed = true) ORDER BY ro.departureDate asc",Trip.class);
        query.setParameter("date", date);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }

    //search trip by StartLocation and Date
    public static Collection<Trip> findByStartLocationAndDate(String departure, Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.startLocation like :departure" +
                " AND ro.departureDate = :date AND (ro.availableSeats > 0 OR t.onlyProposed = true)" +
                " ORDER BY ro.departureDate asc ",Trip.class).setParameter("departure",'%'+departure+'%' ).setParameter("date", date);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }
    //search trip by TargetLocation and Date
    public static Collection<Trip> findByTargetLocationAndDate(String target, Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id " +
                "AND ro.targetLocation like :target AND ro.departureDate = :date AND (ro.availableSeats > 0 " +
                " OR t.onlyProposed = true) ORDER BY ro.departureDate asc",Trip.class);
        query.setParameter("date", date).setParameter("target",'%'+ target+'%' );
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }

    //search trip by startLocation, TargetLocation and Date
    public static Collection<Trip> findByAll(String departure, String target, Date date) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro  WHERE t.id = ro.trip.id " +
                "AND ro.startLocation like :departure AND ro.targetLocation like :target AND ro.departureDate = :date" +
                " AND (ro.availableSeats > 0 OR t.onlyProposed = true) ORDER BY ro.departureDate asc ", Trip.class);
        query.setParameter("date", date).setParameter("departure", '%'+departure+'%').setParameter("target",'%'+target+'%');
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }

    //for related Trips
    //search trip by StartLocation and Date
    //searching for trips from the same startlocation, but different date

    public static Collection<Trip> relatedStartLocationandDate(String departure, Date date, Date datenow) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.startLocation like :departure " +
                "AND ro.departureDate <> :date AND ro.departureDate > :datenow AND (ro.availableSeats > 0 OR t.onlyProposed = true) " +
                "ORDER BY ro.departureDate asc ",Trip.class).setParameter("departure",'%'+departure+'%' ).setParameter("date", date)
                .setParameter("datenow", datenow);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }
    //for related Trips
    //search trip by StartLocation and TargetLocation
    //searching for trips from the same startlocation or to the same target

    public static Collection<Trip> relatedStartLocationandTargetLocation(String departure, String target, Date datenow) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE (t.id = ro.trip.id AND ro.startLocation like :departure " +
                "AND ro.targetLocation not LIKE :target AND (ro.availableSeats > 0 OR t.onlyProposed = true)) AND ro.departureDate > :date  OR (t.id = ro.trip.id AND ro.startLocation not like :departure" +
                " AND ro.targetLocation LIKE :target AND ro.departureDate > :date AND (ro.availableSeats > 0 OR t.onlyProposed = true)) " +
                "ORDER BY ro.departureDate asc ", Trip.class);
                query.setParameter("departure", '%'+departure+'%').setParameter("target", '%'+target+'%').setParameter("date", datenow);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }
    //for related Trips
    //search trip by Date and TargetLocation
    //searching for trips to the same target, but different date

    public static Collection<Trip> relatedTargetLocationandDate(String target, Date date, Date datenow) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE t.id = ro.trip.id AND ro.targetLocation like :target" +
                " AND ro.departureDate <> :date AND ro.departureDate > :datenow AND (ro.availableSeats > 0 OR t.onlyProposed = true) ORDER BY ro.departureDate asc " +
                " ",Trip.class).setParameter("target",'%'+target+'%' ).setParameter("date", date).setParameter("datenow", datenow);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }
    //for related Trips
    //search trip by StartLocation and TargetLocation and Date
    //searching for trips from the same startlocation or to the same target on the same date
    // and for trips on the same date from the start location
    // and for trips on the same date to the target location
    // and for trips between this location on another date


    public static Collection<Trip> relatedTripsAll(String departure, String target, Date date, Date datenow) {
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT distinct t FROM Trip t, Route ro WHERE (t.id = ro.trip.id AND ro.targetLocation like :target" +
                        " AND ro.startLocation not like :departure AND ro.departureDate = :date AND (ro.availableSeats > 0 OR t.onlyProposed = true))" +
                " OR (t.id = ro.trip.id AND ro.startLocation like :departure AND ro.targetLocation not like :target AND ro.departureDate = :date " +
                        " AND (ro.availableSeats > 0 OR t.onlyProposed = true))" +
                        "OR (t.id = ro.trip.id AND ro.startLocation not like :departure AND ro.targetLocation like :target AND ro.departureDate = :date " +
                        " AND (ro.availableSeats > 0 OR t.onlyProposed = true))" +
                        " OR(t.id = ro.trip.id AND ro.startLocation like :departure AND ro.targetLocation like :target AND ro.departureDate > :datenow " +
                        "AND (ro.availableSeats > 0 OR t.onlyProposed = true)) " +
                        " OR (t.id = ro.trip.id AND ro.startLocation like :departure AND ro.targetLocation not like :target and ro.departureDate = :date " +
                        "AND (ro.availableSeats > 0 OR t.onlyProposed = true))" +
                        "AND ro.departureDate > :datenow ORDER BY ro.departureDate asc ",
                Trip.class).setParameter("target",'%'+target+'%' ).setParameter("date", date).setParameter("departure", '%'+departure+'%').setParameter("datenow",datenow);
        List<Trip> trips = query.getResultList();
        if (trips.isEmpty()) {
            return null;
        } else {
            return trips;
        }
    }

    public static Collection<Trip> findFutureTripsByUser(Integer userId, boolean isCreator){
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT DISTINCT t FROM Trip t, TripParticipant tp, Route r " +
                "WHERE t.id = tp.trip.id AND tp.user.id = :id AND tp.isCreator = :isCreator AND r.departureDate > :date " +
                "AND r.trip.id = t.id AND tp.role <> :role order by r.departureDate asc", Trip.class)
                .setParameter("id", userId).setParameter("isCreator", isCreator).setParameter("date", new Date())
                .setParameter("role", Role.pending);
        return query.getResultList();
    }


    public static Collection<Trip> findPastTripsByUserId(Integer userId){
        TypedQuery<Trip> query = JPA.em().createQuery("SELECT DISTINCT t FROM Trip t, TripParticipant tp, Route r " +
                "WHERE t.id = tp.trip.id AND tp.user.id = :id AND r.departureDate < :date AND r.trip.id = t.id " +
                "AND tp.role <> :role AND t.onlyProposed = false order by r.departureDate desc", Trip.class)
                .setParameter("id", userId).setParameter("date", new Date()).setParameter("role", Role.pending);
        return query.getResultList();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public double getCosts() {
        return costs;
    }

    public void setCosts(double costs) {
        this.costs = costs;
    }

    public boolean isGroupActivity() {
        return groupActivity;
    }

    public void setGroupActivity(boolean groupActivity) {
        this.groupActivity = groupActivity;
    }

    public String getIntention() {
        return intention;
    }

    public void setIntention(String intention) {
        this.intention = intention;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isOnlyProposed() {
        return onlyProposed;
    }

    public void setOnlyProposed(boolean onlyProposed) {
        this.onlyProposed = onlyProposed;
    }

    public Collection<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(Collection<Route> routes) {
        this.routes = routes;
    }

    public Collection<TripParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(Collection<TripParticipant> participants) {
        this.participants = participants;
    }



    public void save(){
        JPA.em().persist(this);
        return;
    }

    public void delete() {
        JPA.em().remove(this);
    }

    public Trip update() {
        return JPA.em().merge(this);

    }
}

